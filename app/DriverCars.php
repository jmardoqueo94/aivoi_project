<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverCars extends Model
{
    protected $table = "driver_cars";
}
