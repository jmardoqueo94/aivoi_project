<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddresses extends Model
{
    protected $table = "customer_addresses";
}
