<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfGeneral extends Model
{
    protected $table = "conf_general";
}
