<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspendedUser extends Model
{
    protected $table = "suspended_users";
}
