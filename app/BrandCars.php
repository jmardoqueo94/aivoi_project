<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandCars extends Model
{
    protected $table = "brands_car";
}
