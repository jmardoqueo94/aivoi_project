<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverGeo extends Model
{
    protected $table = "driver_geo";
}
