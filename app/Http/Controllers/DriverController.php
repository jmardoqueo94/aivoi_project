<?php

namespace App\Http\Controllers;

use DB;
use JWTAuth;
use App\User;
use App\Trips;
use App\Driver;
use App\Invoice;
use App\TypeCars;
use App\ModelCars;
use App\BrandCars;
use App\DriverGeo;
use Carbon\Carbon;
use App\DriverCars;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\MetricsController;
use Illuminate\Foundation\Auth\RegistersUsers;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->metrics_controller = new MetricsController;
        $this->mail_controller = new MailController;
    }

    public function saveDriver(Request $request){
        \Log::info($request);

        $driver = new Driver();
        $driver->phone = $request->phone;
        $driver->photo = $request->photo;
        $driver->photo_dui = $request->photo_dui;
        $driver->photo_background = $request->photo_background;
        $driver->photo_solvency_pnc = $request->photo_solvency;
        $driver->user_id = $request->user_id;
        $driver->photo_licence = $request->photo_licence;
        $driver->save();

        if($driver){

            $user_upd = User::find($request->user_id);
            $user_upd->status = 1;
            $user_upd->save();

            $request_data = new Request([
                'name_driver' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'url_create' => 'emails.register_driver',
                'url_alarm' => 'emails.alarm_new_driver',
            ]);

            $this->mail_controller->createAccountDriver($request_data);
            $this->mail_controller->alarmNewDriver($request_data);

            return $driver;
        }else{
            return 'error';
        }
    }

    public function getTypeCars(){

        try {
            $types = TypeCars::get();

            if($types){
                return response()->json([
                    'data' => $types,
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function geoDriver(Request $request){
        Log::info("LLEGA A GEODRIVER");
        Log::info($request);

        $driver_id = $request->driver_id;
        $driver_player_id = $request->driver_player_id;
        $data_geo = DriverGeo::where('user_id', '=', $driver_id)->get();
        $count_geo = $data_geo->count();

        if (isset($count_geo) && $count_geo == 0) {
            // It does not exist
            $geo = new DriverGeo();
            $geo->user_id = $driver_id;
            $geo->lat = $request->lat;
            $geo->lng = $request->lng;
            $geo->driver_player_id = $driver_player_id;
            $geo->save();
        } else {
            // It exists 
            $geo = DriverGeo::where('user_id', $driver_id)
            ->update([
                'lat' => $request->lat, 
                'lng' => $request->lng,
                'driver_player_id' => $driver_player_id,
            ]);
        }

        if($geo){
            return "geo driver actualizado";
        }else{
            return "geo driver no actualizado";
        }
    }

    public function updateDriver(Request $request){
        \Log::info('UPD DRIVER');
        \Log::info($request);
        $data = $request->all();
        $user_id = $request->user_id;
        
        foreach ($data as $key => $value) {

            if($key == 'name'){
                $driver[] = User::where('id', $user_id)
                ->update([
                    $key => $value,
                ]);
            }elseif($key == 'phone' || $key == 'photo'){
                $driver[] = Driver::where('user_id', $user_id)
                ->update([
                    $key => $value,
                ]);
            }
        }
        
        if(count($driver) > 0){
            return response()->json([
                'data' => array('message' => 'Datos actualizados correctamente')
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => 'No se pudieron actualizar los datos')
            ], 400);
        }
    }

    public function addCarDriver(Request $request){

        $user_id = $request->user_id;

        $driver_id = Driver::where('user_id', $user_id)->pluck('id')->toArray();

        $cars = DriverCars::where('id_driver', $driver_id)->get();
        $cars = json_decode(json_encode($cars), true);

        foreach($cars as $row){
            $cars_driver = DriverCars::find($row['id']);
            $cars_driver->status = 0;
            $cars_driver->save();
        }

        $car = new DriverCars();
        $car->model_car_id = $request->model_car_id;
        $car->brand_car_id = $request->brand_car_id;
        $car->placa = $request->placa;
        $car->type_car_id = $request->type_car_id;
        $car->year = $request->year;
        $car->color = $request->color;
        $car->id_driver = $driver_id[0];
        $car->status = 1;
        $car->photo_card_circulation = isset($request->photo_card_circulation) ? $request->photo_card_circulation : null;
        $car->save();

        if($car){
            return response()->json([
                'data' => $car,
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No se pudo guardar el vehículo")
            ], 400);
        }
    }

    public function updateCarDriver(Request $request){
        $user_id = $request->user_id;
        $driver_id = Driver::where('user_id', $user_id)->pluck('id')->toArray();

        $data = $request->all();
        $user_id = $request->user_id;
        $id_car = $request->id;

        if(isset($request->status) && $request->status == 1){
            $cars = DriverCars::where('id', '!=', $id_car)
                ->where('id_driver', $driver_id)
                ->get();

            $cars = json_decode(json_encode($cars), true);

            if(count($cars) >= 1){
                foreach($cars as $row){
                    $cars_driver = DriverCars::find($row['id']);
                    $cars_driver->status = 0;
                    $cars_driver->save();
                }
            }
        }
        
        if(isset($request->id)){

            foreach ($data as $key => $value) {
                if($key != 'user_id' && $key != 'token' && $key != 'id'){
                    $car[] = DriverCars::where('id', $id_car)
                    ->update([
                        $key => $value,
                    ]);
                }
            }

            if($car){
                return response()->json([
                    'data' => array('message' => 'Se actualizó correctamente')
                ], 200);
            }else{
                return response()->json([
                    'data' => array('message' => "No se actualizó")
                ], 400);
            }
        }else{
            return response()->json([
                'data' => array('message' => "No se identificó el vehiculo")
            ], 400);
        }
    }

    public function getDriversNear(Request $request){
        \Log::info('ESTO LLEGA A NEAR');
        \Log::info($request);
        try {

            $lat = $request->lat;
            $lon = $request->lng;
            $distance = 2;

            $drivers = DB::select("
            SELECT users.name, driver_geo.id, driver_geo.user_id, driver_geo.driver_player_id, driver_geo.lat, driver_geo.lng, driver.photo, driver.id as driver_id, driver_geo.created_at, driver_geo.updated_at FROM (
                SELECT *, 
                    (
                        (
                            (
                                acos(
                                    sin(( $lat * pi() / 180))
                                    *
                                    sin(( lat * pi() / 180)) + cos(( $lat * pi() /180 ))
                                    *
                                    cos(( lat * pi() / 180)) * cos((( $lon - lng) * pi()/180)))
                            ) * 180/pi()
                        ) * 60 * 1.1515 * 1.609344
                    )
                as distance FROM driver_geo
            ) driver_geo 
            INNER JOIN users
                ON driver_geo.user_id = users.id
            INNER JOIN driver
                ON users.id = driver.user_id
            WHERE users.busy = 0 AND distance <= $distance
            LIMIT 15");
            //WHERE AND users.online = 1 AND distance <= $distance  
            if($drivers){
                $drivers = json_decode(json_encode($drivers), true);
                $now = Carbon::now()->subMinutes(30)->toDateTimeString();

                foreach ($drivers as $key => $value) {

                    $km = $this->metrics_controller->distanceCalculation($lat, $lon, $value['lat'], $value['lng']); // Calcular la distancia en kilómetros (por defecto)
                    $drivers[$key]['distance'] = $km;
                    
                    /*if($value['updated_at'] < $now){
                        unset($drivers[$key]);
                    }*/
                }
                $res = array_values($drivers);
                return response()->json([
                    'data' => $res,
                ], 200);
            }else{
                return response()->json([
                    'data' => array('message' => "No hay drivers cerca")
                ], 200);
            }
            
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function getDriverCars(Request $request){
        try {
            $user_id = $request->user_id;
            $driver_id = Driver::where('user_id', $user_id)->pluck('id')->toArray();

            $cars = DriverCars::join('brands_car', 'brands_car.id', '=', 'driver_cars.brand_car_id')
                    ->join('models_car', 'models_car.id', '=', 'driver_cars.model_car_id')
                    ->where('id_driver', $driver_id)
                    ->get();

            if($cars){
                return response()->json([
                    'data' => $cars,
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function deleteCarDriver(Request $request){
        $car_id = $request->id;
        
        $car = DriverCars::find($car_id);
        $car->delete();

        if($car){
            return response()->json([
                'data' => array('message' => "Vehiculo eliminado correctamente")
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No se pudo eliminar el vehiculo")
            ], 400);
        }
    }

    public function getTripsDriver(Request $request){
        try {

            $driver_id = $request->id;

            $trips = Invoice::where('driver_id', $driver_id)
            ->orderBy('created_at', 'desc')
            ->get(['id', 'driver_id', 'bill_trip','price AS cost', 'origin_lat', 'origin_lng', 'destination_lat', 'destination_lng', 'created_at', 'name_driver AS driver_name']);

            if($trips){

                foreach ($trips as $key => $value) {
                    $km = $this->metrics_controller->distanceCalculation($value->origin_lat, $value->origin_lng, $value->destination_lat,  $value->destination_lng); // Calcular la distancia en kilómetros (por defecto)
                    $trips[$key]['distance'] = $km * 1000;
                }

                return response()->json([
                    'data' => $trips,
                ], 200);
            }else{
                return response()->json([
                    'data' => [],
                ], 200);
            }
            
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function updateDocumentsDriver(Request $request){
        \Log::info('UPDT DOCS');
        \Log::info($request);
        
        $data = $request->all();
        $user_id = $request->user_id;
        
        foreach ($data as $key => $value) {
            if($key != 'user_id' && $key != 'token'){
                $driver[] = Driver::where('user_id', $user_id)
                ->update([
                    $key => $value,
                ]);
            }
        }
        
        if($driver){
            return response()->json([
                'data' => array('message' => 'Datos actualizados correctamente')
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => 'No se pudieron actualizar los datos')
            ], 400);
        }
    }

    public function onlineDriver(Request $request){
        $user_id = $request->id_user;
        $status = $request->status;

        $user = User::where('id', $user_id)
            ->update([
                'online' => $status,
            ]);

        if($user){
            return response()->json([
                'data' => array('message' => 'Se cambio status del driver')
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => 'No se pudieron cambiar status del driver')
            ], 400);
        }
    }

    public function getTotalBalanceDriver(Request $request){

        $date = Carbon::today();
        $driver_id = $request->driver_id;

        $trips = Invoice::select("origin_lat", "origin_lng", "destination_lat", "destination_lng", "price")
            ->where('driver_id', $driver_id)->whereDate('created_at','>=', $date)
            ->groupBy('id')
            ->get();

        $sum_trips = count($trips);
        
        if($sum_trips > 0){
            $sum_cost = 0;
            $sum_distance = 0;

            foreach ($trips as $key => $value) {
                $km = $this->metrics_controller->distanceCalculation($value->origin_lat, $value->origin_lng, $value->destination_lat,  $value->destination_lng); // Calcular la distancia en kilómetros (por defecto)
                $sum_distance += $km;
                $sum_cost += $value->price;
            }

            $sum_distance2= number_format((float)$sum_distance, 2, '.', '').' km';
            $sum_cost_total = number_format((float)$sum_cost, 2, '.', '');

            return response()->json([
                'data' => array("balance" => $sum_cost_total, "total_trips" => $sum_trips, "distance" => $sum_distance2)
            ], 200);
        }else{
            return response()->json([
                'data' => [],
            ], 200);
        }
    }

    public function getModelsCars(Request $request){
        try {
            $brands = BrandCars::get();
            $models = ModelCars::get();

            if($brands && $models){
                return response()->json([
                    'data' => array('brands' => $brands, 'models' => $models),
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }
}
