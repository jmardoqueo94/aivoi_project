<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Trips;
use App\Driver;
use App\TripsLogs;
use App\PriceVariants;

class MetricsController extends Controller
{
    public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
        // Cálculo de la distancia en grados
        $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
    
        // Conversión de la distancia en grados a la unidad escogida (kilómetros, millas o millas naúticas)
        switch($unit) {
            case 'km':
                $distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
                break;
        }

        return round($distance, $decimals);
    }

    public function distancia(Request $request){
        
        $trip = DB::select("
            SELECT COUNT(*) as carreras, FORMAT(SUM(cost),2) as total, driver_id FROM trips INNER JOIN users ON users.id = trips.driver_id where users.id= $request->id ORDER BY users.id DESC;
        ");

        $array_trip = json_decode(json_encode($trip), true);
        $distance = Trips::where('driver_id', $request->id)->get();

        $distance_array = json_decode(json_encode($distance), true);
        $new_dist = 0;

        if(isset($distance_array) && !empty($distance_array)){
            foreach ($distance_array as $key => $value) {
                $km = $this->distanceCalculation($value['origin_lat'], $value['origin_lng'], $value['destination_lat'], $value['destination_lng']); // Calcular la distancia en kilómetros (por defecto)
                $new_dist += $km;
            }
    
            $array_trip[0]['distance'] = number_format((float)$new_dist, 2, '.', '').' km';
        }

        if(!empty($array_trip)){
            return response()->json([
                'data' => $array_trip,
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No existen viajes de dicho conductor")
            ], 400);
        }
    }

    public function generatePrice(Request $request){

        $prices = PriceVariants::get();

        if(count($prices) > 0){
            $distance = $request->distance;
            $time_sec = $request->time;
            $card = $request->card;

            foreach ($prices as  $value) {
                $cost_traffic = $value->traffic;
                $hours = floor($time_sec / 3600);
                $time_min =  floor(($time_sec - ($hours * 3600)) / 60);
                $total_time = $time_min * $value->min;
                
                $distance_km = $distance / 1000;
                $base = $value->base;

                $comission_service = $card == 0 ? 0 : $value->credit_card;

                if($distance_km <= 20){
                    $val_km = $value->short_distance;
                    $price_km = $distance_km * $val_km;
                    $total_price = $total_time + $base + $price_km + $comission_service + $cost_traffic;

                }else{
                    $val_km = $value->long_distance;
                    $price_km = $distance_km * $val_km;
                    $total_price = $total_time + $base + $price_km + $comission_service + $cost_traffic;
                }
            }

            return response()->json([
                'data' => number_format($total_price, 2, '.', '')
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => 'Ocurrió un error al obtener las métricas')
            ], 400);
        }
    }

}
