<?php

namespace App\Http\Controllers;

use App\Payment;

use JWTAuth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;


class PaymentController extends Controller
{
    public function getPayments(Request $request){
        try {
            $payment = Payment::where('status', 1)->get();

            $payment[0]['url_icon'] = "https://res.cloudinary.com/grupo-dicase/image/upload/v1614625917/aivoi%20app%20resourses/cash_tcuerw.png";

            if($payment){
                return response()->json([
                    'data' => $payment,
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function getBanks(){
        $banks = DB::table('banks')->get();

        if($banks){
            return response()->json([
                'data' => $banks,
            ], 200);
        }else{
            return response()->json([
                'data' => []
            ], 200);
        }
    }
}
