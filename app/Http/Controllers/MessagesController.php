<?php

namespace App\Http\Controllers;
use App\User;
use App\Driver;
use App\DriverCars;
use App\Messages;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MessagesController extends Controller
{
    
    public function saveMessage(Request $request){

        \Log::info($request);
        $msg = new Messages();
        $msg->from_user = $request->from_user;
        $msg->to_user = $request->to_user;
        $msg->content = $request->content;
        $msg->trip_id = $request->trip_id;
        $msg->save();

        if($msg){
            return "mensaje enviado";
        }else{
            return "mensaje no fue enviado";
        }
    }

    public function getMessages(Request $request){

        try {

            $msg = Messages::where('trip_id', $request->trip_id)->get();
        
            if($msg){
                return response()->json([
                    'data' => $msg,
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

}
