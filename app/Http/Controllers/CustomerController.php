<?php

namespace App\Http\Controllers;
use App\User;
use App\Driver;
use App\DriverCars;
use App\DriverGeo;
use App\Customer;
use App\Trips;
use App\Invoice;
use App\CustomerAddresses;
use JWTAuth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Mail\TestEmail;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\MailController;
use App\Http\Controllers\MetricsController;
use Carbon\Carbon;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->mail_controller = new MailController;
        $this->metrics_controller = new MetricsController;
    }

    public function saveCustomer(Request $request){

        $customer = new Customer();
        $customer->user_id = $request->user_id;
        $customer->phone = $request->phone;
        $customer->photo = isset($request->photo) ? $request->photo : null;
        $customer->info_device = $request->info_device;
        $customer->save();

        if($customer){

            $user_upd = User::find($request->user_id);
            $user_upd->status = 1;
            $user_upd->save();

            $request_data = new Request([
                'name_customer' => $request->name,
                'email' => $request->email,
                'url' => 'emails.account',
            ]);

            $this->mail_controller->createAccount($request_data);

            return $customer;
        }else{
            return 'error';
        }
    }

    public function saveAddress(Request $request){

        $user = JWTAuth::parseToken()->authenticate();

        $customer_id = Customer::where('user_id', $user->id)->pluck('id')->toArray();
        
        $address = new CustomerAddresses();
        $address->customer_id = $customer_id[0];
        $address->name_address = $request->name_address;
        $address->lat_address = $request->lat_address;
        $address->lng_address = $request->lng_address;
        $address->save();

        if($address){
            return response()->json([
                'data' => $address
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No se pudo guardar la dirección")
            ], 400);
        }
    }

    public function getCustomerAddresses(Request $request){

        try {

            $user = JWTAuth::parseToken()->authenticate();
            $customer_id = Customer::where('user_id', $user->id)->pluck('id')->toArray();

            $addresses = CustomerAddresses::where('customer_id', $customer_id[0])->get();

            if($addresses){
                return response()->json([
                    'data' => $addresses,
                ], 200);
            }else{
                return response()->json([
                    'data' => array('message' => "No hay direcciones guardadas")
                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }        
    }

    public function updateCustomer(Request $request){
        \Log::info('UPD CUSTOMER');
        \Log::info($request);

        $data = $request->all();
        $user_id = $request->user_id;
        
        foreach ($data as $key => $value) {

            if($key == 'name'){
                $customer[] = User::where('id', $user_id)
                ->update([
                    $key => $value,
                ]);
            }elseif($key == 'phone' || $key == 'photo'){
                $customer[] = Customer::where('user_id', $user_id)
                ->update([
                    $key => $value,
                ]);
            }
        }

        if(count($customer) > 0){
            return response()->json([
                'data' => array('message' => 'Datos actualizados correctamente')
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => 'No se pudieron actualizar los datos')
            ], 400);
        }
    }

    public function updateAddress(Request $request){
                
        $address_id = $request->id;
        
        $address = CustomerAddresses::find($address_id);
        $address->name_address = $request->name_address;
        $address->lat_address = $request->lat_address;
        $address->lng_address = $request->lng_address;
        $address->save();

        if($address){
            return response()->json([
                'data' => $address
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No se pudo actualizar la dirección")
            ], 400);
        }
    }

    public function deleteAddress(Request $request){
        
        $address_id = $request->id;
        
        $address = CustomerAddresses::find($address_id);
        $address->delete();

        if($address){
            return response()->json([
                'data' => array('message' => "Dirección eliminada correctamente")
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No se pudo eliminar la dirección")
            ], 400);
        }
    }

    public function getTripsCustomer(Request $request){

        try {
            $customer_id = $request->id;

            $email = User::where('id', $customer_id)->pluck('email')->toArray();

            $trips = Invoice::where('email_customer', $email[0])
                    ->orderBy('created_at', 'desc')
                    ->get(['id', 'driver_id', 'bill_trip','price AS cost', 'origin_lat', 'origin_lng', 'destination_lat', 'destination_lng', 'created_at', 'name_driver AS driver_name']);

            if($trips){
                foreach ($trips as $key => $value) {
                    $km = $this->metrics_controller->distanceCalculation($value->origin_lat, $value->origin_lng, $value->destination_lat,  $value->destination_lng); // Calcular la distancia en kilómetros (por defecto)
                    $trips[$key]['distance'] = $km * 1000;
                }
                return response()->json([
                    'data' => $trips,
                ], 200);
            }else{
                return response()->json([
                    'data' => [],
                ], 200);
            }
        
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }
}
