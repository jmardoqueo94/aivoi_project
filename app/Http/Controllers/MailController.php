<?php

namespace App\Http\Controllers;

use App\Mail\TestEmail;
use App\Mail\PruebaMail;
use App\Mail\RegisterDriver;
use App\Mail\ConfirmDriver;
use App\Mail\TotalUsers;
use App\Mail\AlarmDriver;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class MailController extends Controller
{
    public function createAccount(Request $request){
        $email = $request->email;
        $data = [
            'message' => '¿A dónde quieres ir hoy? '."\n".' Tu planea tus viajes, nosotros te llevamos.', 
            'name' => 'Info AIVOI', 
            'subject' => "Gracias por registrarte",
            'name_customer' => $request->name_customer,
            'url' => $request->url
        ];

        Mail::to($email)->send(new TestEmail($data));
    }

    public function billTripMail(Request $request){
        
        $data = "bill";

        $email = $request->email;
        $data = [
            'message' => 'This is a test!', 
            'name' => 'Info AIVOI', 
            'subject' => "Gracias por registrarte",
            'name_customer' => $request->name_customer,
            'mail' => $request->url
        ];

        Mail::to($email)->send(new TestEmail($data));
    }

    public function restorePassword(Request $request){
        
        $data = [
            'name' => 'Info AIVOI', 
            'subject' => "Restauración de contraseña",
            'message' => $request->password,
            'name_customer' => $request->name_customer,
            'password' => $request->password,
            'url' => $request->url
        ];

        Mail::to($request->email)->send(new TestEmail($data));
    }

    public function createAccountDriver(Request $request){
        \Log::info("LLEGA A CREAR CUENTA DRIVER");
        \Log::info($request);
        $email = $request->email;
        $data = [
            'message' => 'Hemos recibido tu documentación para revisión, espera 48 horas hábiles para que tu cuenta se active y recibirás un email informándote al respecto, de lo contrario, alguien de soporte se comunicara contigo explicándote la razón de no haber sido activada tu cuenta', 
            'name' => 'Info AIVOI', 
            'subject' => "Gracias por registrarte",
            'name_driver' => $request->name_driver,
            'url_create' => $request->url_create
        ];

        Mail::to($email)->send(new RegisterDriver($data));
    }

    public function confirmAccountDriver(Request $request){
        \Log::info("LLEGA A CONFIRMAR CUENTA DRIVER");
        \Log::info($request);
        $email = $request->email;
        $data = [
            'message' => '¡Felicidades! - Tu cuenta ha sido activada, bienvenido a la familia de AIVOI', 
            'name' => 'Info AIVOI', 
            'subject' => "Cuenta Activada",
            'name_driver' => $request->name_driver,
            'url' => $request->url
        ];

        Mail::to($email)->send(new ConfirmDriver($data));
    }

    public function alarmNewDriver(Request $request){
        \Log::info("LLEGA A ALARMA DRIVER");
        \Log::info($request);
        $email = 'backend@aivoiapp.com';
        $data = [
            'message' => 'Se ha registrado un nuevo driver, en el perfil administrador se puede ver el detalle.', 
            'name' => 'Info AIVOI', 
            'subject' => "Nuevo driver",
            'name_driver' => $request->name_driver,
            'phone' => $request->phone,
            'url_alarm' => $request->url_alarm
        ];

        Mail::to($email)->send(new AlarmDriver($data));
    }

    public function TotalUsers(Request $request){
        $email = 'backend@aivoiapp.com';
        $data = [
            'message' => 'Se han registrado nuevos usuarios.', 
            'name' => 'Info AIVOI', 
            'subject' => "Nuevos usuarios"
        ];

        Mail::to($email)->send(new TotalUsers($data));
    }
}
