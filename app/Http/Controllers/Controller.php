<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function testNotification(Request $request){
        $id = $request->id;
        $message = $request->message;

        $content = array(
            "en" => $message, 
        );
    
        $dat= array(
            "prueba" => Carbon::now()
        );
    
        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=>array($id),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic ODk3MmFiMTctYWEyMi00ODFmLTgwM2QtZTUzMmQzYjQ3NDE1'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
    
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            Log::info("cURL Error #:" . $err);
            return $err;
        } else {
            return response()->json([
                'success' => true,
                'message' => "Sent",
                'allresponses' => $response,
            ], 200);
        }
    }
}
