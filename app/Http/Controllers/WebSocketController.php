<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\TripsController;
use JWTAuth;
use App\Driver;
use App\Customer;
use App\Messages;

class WebSocketController extends Controller implements MessageComponentInterface
{
    private $connections = [];
    private $providers = []; // [$Person|Driver|Customer]

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $messages_ctrl = new MessagesController;
        $driver_ctrl = new DriverController;
        $trips_ctrl = new TripsController;

        Log::info('Esto viene del cliente blob');

        //Evaluare sobre que es la peticion si chat o geo
        $blob = json_decode($msg);

        if(isset($blob->{'channel'})){
            if($blob->{'channel'} == "identify"){
                \Log::info("LLEGA AL IDENTIFY");
                \Log::info($msg);

                $model_id = $blob->{'user_id'};
                $model_name = $blob->{'user_name'};
                $uuid = $blob->{'uuid'};
                
                $exist = array_search($model_id, array_column($this->providers, 'id'));

                if($exist === FALSE){
                    array_push($this->providers, array(
                        'id' => $model_id,
                        'name' => $model_name,
                        'connection' => $from,
                        'uuid' => $uuid,
                    ));

                    $from->send(json_encode(array(
                        'message' => 'Identify completed',
                        'channel' => 'identify'
                    )));
                }else{
                    $currentValue = $this->providers[$exist];
                    $this->providers[$exist] = array(
                        'id' => $currentValue['id'],
                        'name' => $currentValue['name'],
                        'connection' => $from,
                        'uuid' => $uuid
                    );

                    $from->send(json_encode(array(
                        'message' => 'Identify updated',
                        'channel' => 'identify'
                    )));
                }
            }
            elseif($blob->{'channel'} == "chat"){
                \Log::info("LLEGA AL CHAT");
                \Log::info($msg);
                
                $from_user = $blob->{'from_user'};
                $to_user = $blob->{'to_user'};
                $bill_trip = $blob->{'bill_trip'};

                $message = $blob->{'message'};
                $user_one_id = $blob->{'user_customer_id'}; // 551
                $user_two_id = $blob->{'user_driver_id'}; // 34
                $user_one_index = array_search($user_one_id, array_column($this->providers, 'id'));
                $user_two_index = array_search($user_two_id, array_column($this->providers, 'id'));

                $user_one = $this->providers[$user_one_index]; // Customer
                $user_two = $this->providers[$user_two_index]; // Driver

                $request_chat = new Request([
                    'from_user'   => $from_user,
                    'to_user'     => $to_user,
                    'content'     => $message,
                    'trip_id'     => $blob->{'trip_id'},
                ]);

                $response = $messages_ctrl->saveMessage($request_chat);

                if ($from === $user_one['connection']) {
                    \Log::info("entra al primer if");
                    $date = date('Y-m-d H:i:s');
                    $user_two['connection']->send(json_encode(array(
                        'message' => $message,
                        'channel' => 'chat',
                        'date' => $date
                    )));

                    
                } elseif ($from === $user_two['connection']) {
                    \Log::info("entra al else from === user");
                    $date = date('Y-m-d H:i:s');
                    $user_one['connection']->send(json_encode(array(
                        'message' => $message,
                        'channel' => 'chat',
                        'date' => $date
                    )));

                }
            }
            elseif($blob->{'channel'} == "tracking"){

                $request_geo = new Request([
                    'driver_id'   => $blob->{'driver_id'},
                    'lat'     => $blob->{'lat'},
                    'lng'     => $blob->{'lng'},
                    'driver_player_id'     => $blob->{'uuid'},
                    'status' => $blob->{'status'},
                ]);

                $response = $driver_ctrl->geoDriver($request_geo);

                //Verificamos si el driver tiene viaje y le enviamos la información de tracking al cliente
                $driver_tracking = $trips_ctrl->verifyTripInfo($blob->{'driver_id'});

                if($driver_tracking != 0){
                    $customer_id = $driver_tracking[0]->customer_id;
                    $user_one_id = $customer_id; // 551
                    $user_one_index = array_search($user_one_id, array_column($this->providers, 'id'));
                    $user_one = $this->providers[$user_one_index]; // Customer

                    $user_one['connection']->send(json_encode(array(
                        'tracking' => array('lat' => $blob->{'lat'}, 'lng' => $blob->{'lng'},),
                        'channel' => 'tracking_driver'
                    )));
                }else{
                    $newMessage = array(
                        'text' => $response,
                        'local' => false,
                        'updatedFromServer' => true,
                    );
        
                    $from->send(json_encode($newMessage));
                }
            }
            elseif($blob->{'channel'} == "notifications"){
                $uuid = $blob->{'uuid'};
                $type = $blob->{'type'};
                
                $user_uuid = array_search($uuid, array_column($this->providers, 'uuid'));
                $user_device = $this->providers[$user_uuid]; // Driver

                $array = (array) $blob;

                $request = new Request([
                    'parameters'   => $array
                ]);

                $response = $trips_ctrl->$type($request);
    
                $user_device['connection']->send(json_encode($response));
            }
            elseif($blob->{'channel'} == "testing"){

                $test = $blob->{'test'};

                $from->send(json_encode($test));
            }
        }else{
            $newMessage = array(
                'text' => "Estructura de datos incorrecta",
                'local' => false,
                'updatedFromServer' => true,
            );

            $from->send(json_encode($newMessage));
        }

    }

    function onOpen(ConnectionInterface $conn)
    {
        array_push($this->connections, $conn);
        Log::info('An user was entered to the connection');
    }

    function onClose(ConnectionInterface $conn)
    {
        Log::info('Connection closed');
    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {
        Log::error($e->getMessage());
        $conn->close();
    }
}
