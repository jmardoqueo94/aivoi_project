<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\User;
use App\Trips;
use App\TripsLogs;
use App\DriverCars;
use App\Driver;
use App\DriverGeo;
use App\Customer;
use App\TripsCancelled;
use App\UserBalance;
use App\RatingUser;
use Carbon\Carbon;
use App\Providers\RouteServiceProvider;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class TripsController extends Controller
{

    public function createTrip(Request $request){
        \Log::info($request);
        //$driver_id = Driver::where('id', $request->driver_id)->pluck('user_id')->toArray();
        $driver_id = $request->driver_id;

        $busy_driver = User::where('id', $driver_id)->pluck('busy')->toArray();

        if($busy_driver[0] == 0){//Significa que el driver no esta en viaje

            $balance_negative = UserBalance::where('id_user', $request->customer_id)->pluck('negative_balance')->toArray();
            $balance_negative2 = isset($balance_negative[0]) ? $balance_negative[0] : false;

            if ($balance_negative2){
                $cost_trip = $request->cost + $balance_negative[0];//despues tengo que poner en  el saldo negativo
                $remaining = 0;
                $balance = UserBalance::where('id_user', $request->customer_id)
                            ->update(['negative_balance' => $remaining]);
            }else{
                $cost_trip = $request->cost;
            }

            if($request->credits_balance == 1){
                $positive_balance = UserBalance::where('id_user', $request->customer_id)->pluck('positive_balance')->toArray();
                $positive_balance2 = isset($positive_balance[0]) ? $positive_balance[0] : false;
                
                if ($positive_balance2){
                    if($positive_balance[0] >= $request->cost){
                        $remaining = $positive_balance[0] - $request->cost;
                        $cost_trip = 0;//luego de esto tengo que restarle el precio al balance y dejar lo restante en la tabla
                        
                        $balance = UserBalance::where('id_user', $request->customer_id)
                            ->update(['positive_balance' => $remaining]);
                    }else{
                        $cost_trip = $request->cost - $positive_balance[0];//aqui si debo poner en 0 el balance
                    }
                }else{
                    $cost_trip = $request->cost;
                }
            }

            $trip = new Trips();
            $trip->bill_trip = Str::random(10);
            $trip->driver_id = $driver_id;
            $trip->customer_id = $request->customer_id;
            $trip->origin_lat = $request->origin_lat;
            $trip->origin_lng = $request->origin_lng;
            $trip->destination_lat = $request->destination_lat;
            $trip->destination_lng = $request->destination_lng;
            $trip->cost = $cost_trip;
            $trip->payment_id = $request->payment_id;
            $trip->customer_player_id = $request->customer_player_id;
            $trip->driver_player_id = $request->driver_player_id;
            $trip->save();

            if($trip){

                //Se creará el primer status del viaje
                $log_trip = new TripsLogs();
                $log_trip->trip_id = $trip->id;
                $log_trip->status_trip_id = 1;//es estado SOLICITADO
                $log_trip->save();

                $driver_car_id = Driver::where('user_id', $request->driver_id)->pluck('id')->toArray();
                $car_driver = DriverCars::join('brands_car', 'brands_car.id', '=', 'driver_cars.brand_car_id')
                    ->join('models_car', 'models_car.id', '=', 'driver_cars.model_car_id')
                    ->where('id_driver', $driver_car_id[0])
                    ->where('status', 1)
                    ->get();

                $customer_info = Customer::join('users', 'users.id', '=', 'customer.user_id')
                    ->where('customer.user_id', $request->customer_id)
                    ->get(['users.id', 'users.name', 'customer.phone', 'customer.photo']);

                return response()->json([
                    'data' => array($trip, $car_driver, $customer_info)
                ], 200);
            }else{
                return response()->json([
                    'data' => array('message' => 'No se pudo crear el viaje')
                ], 400);
            }
        }else{
            return response()->json([
                'data' => array('message' => "El conductor ya tiene un viaje activo")
            ], 400);
        }
    }

    public function requestTrip(Request $request){
        \Log::info('NOTIFICACION NUEVO VIAJE');
        \Log::info($request);
        $driver_player_id = $request->parameters['uuid'];
        $customer_user_id = $request->parameters['customer_id'];
        $bill_trip = $request->parameters['bill_trip'];

        $driver_trip = Trips::where('bill_trip', $bill_trip)->pluck('driver_id')->toArray();
        $player_geo_driver = DriverGeo::where('user_id', $driver_trip[0])->pluck('driver_player_id')->toArray();
        $driver_player_id = ($player_geo_driver[0] == $driver_player_id) ? $driver_player_id : $player_geo_driver[0];
        //DEBO REVISAR POR QUE NO SE MANDA ESTE INFO
        $customer_info = Customer::join('users', 'users.id', '=', 'customer.user_id')
            ->where('customer.user_id', $customer_user_id)
            ->get(['users.id', 'users.name', 'customer.phone', 'customer.photo']);

        $content = array(
            "en" => 'Nueva solicitud de viaje', 
        );

        $trip = Trips::join('users', 'users.id', '=', 'trips.customer_id')
                ->join('customer', 'customer.user_id', '=', 'users.id')
                ->join('payments', 'payments.id', '=', 'trips.payment_id')
                ->where('bill_trip', $bill_trip)
                ->where('users.id', $customer_user_id)
                ->get(['customer.photo', 'users.name', 'payments.type', 'trips.id', 'trips.customer_player_id', 'trips.customer_id', 'trips.bill_trip', 'customer.user_id AS to_user','trips.cost', 'trips.origin_lat', 'trips.origin_lng', 'trips.destination_lat', 'trips.destination_lng', 'trips.created_at']);
        
        $dat= array(
            "factura" => $bill_trip,
            "trip" => $trip,
            "customer_id" => $customer_user_id,
            "customer_info" => $customer_info
        );

        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=> array($driver_player_id),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content,
            'type' => 'notification/requestTrip'
        );

        return $fields;
    }

    public function acceptTrip(Request $request){
        \Log::info("SE ACEPTO EL VIAJE");
        $customer_player_id = $request->parameters['uuid'];
        $bill_trip = $request->parameters['bill_trip'];
        $trip_id = $request->parameters['trip_id'];

        //Se cambiara estado del viaje a aceptado
        $log_trip = new TripsLogs();
        $log_trip->trip_id = $trip_id;
        $log_trip->status_trip_id = 2;//es estado ACEPTADO
        $log_trip->save();

        $driver_busy = User::where('id', $request->parameters['driver_id'])
        ->update([
            'busy' => 1,
        ]);

        $content = array(
            "en" => 'El conductor aceptó tu viaje, espera en el punto de partida.', 
        );
    
        $dat= array(
            "factura" => $bill_trip,
            "type" => "3"
        );
    
        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=>array($customer_player_id),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content,
            'type' => 'notification/acceptTrip'
        );

        \Log::info($fields);

        return $fields;
    }

    public function waitTrip(Request $request){
        \Log::info('LLEGA A WAIT TRIP');
        $customer_player_id = $request->parameters['uuid'];
        $bill_trip = $request->parameters['bill_trip'];

        $content = array(
            "en" => 'El conductor va encamino, esperalo en el punto de partida...', 
        );
    
        $dat= array(
            "factura" => $bill_trip
        );
    
        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=>array($customer_player_id),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content,
            'type' => 'notification/waitTrip'
        );

        \Log::info($fields);
        
        return $fields;
    }

    public function initTrip(Request $request){
        \Log::info('LLEGA A INIT TRIP');
        $customer_player_id = $request->parameters['uuid'];
        $bill_trip = $request->parameters['bill_trip'];
        $trip_id = $request->parameters['trip_id'];

        //Se cambiara estado del viaje a en camino
        $log_trip = new TripsLogs();
        $log_trip->trip_id = $trip_id;
        $log_trip->status_trip_id = 3;//es estado EN CAMINO
        $log_trip->save();

        $content = array(
            "en" => 'El viaje inició, esperamos que llegues con bien a tu destino', 
        );
    
        $dat= array(
            "factura" => $bill_trip
        );
    
        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=>array($customer_player_id),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content,
            'type' => 'notification/initTrip'
        );

        \Log::info($fields);

        return $fields;
    }
    
    public function finishTrip(Request $request){
        \Log::info("LLEGA A FINISH TRIP");
        $customer_player_id = $request->parameters['uuid'];
        $bill_trip = $request->parameters['bill_trip'];
        $trip_id = $request->parameters['trip_id'];

        //Se cambiara estado del viaje a completado
        $log_trip = new TripsLogs();
        $log_trip->trip_id = $trip_id;
        $log_trip->status_trip_id = 4;//es estado COMPLETADO
        $log_trip->save();

        $driver_busy = User::where('id', $request->parameters['driver_id'])
        ->update([
            'busy' => 0,
        ]);

        $content = array(
            "en" => 'Has realizado tu viaje con éxito', 
        );
    
        $dat= array(
            "factura" => $bill_trip,
            "type" => "1"
        );
    
        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=>array($customer_player_id),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content,
            'type' => 'notification/finishTrip'
        );

        \Log::info($fields);

        return $fields;
    }

    public function payConfirmation(Request $request){
        $customer_player_id = $request->parameters['uuid'];
        $bill_trip = $request->parameters['bill_trip'];
        $trip_id = $request->parameters['trip_id'];

        $driver_busy = User::where('id', $request->parameters['driver_id'])
            ->update([
                'busy' => 0, 
            ]);

        $content = array(
            "en" => 'Gracias por viajar con Aivoi', 
        );
    
        $dat= array(
            "factura" => $bill_trip,
            "type" => "1"
        );
    
        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=>array($customer_player_id),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content,
            'type' => 'notification/payConfirmation'
        );

        return $fields;
    }

    public function cancelTrip(Request $request){
        \Log::info("LLEGA A CANCEL TRIP");
        \Log::info($request);
        $user_id = $request->parameters['user_id'];
        $user_name = $request->parameters['user_name'];
        
        $player_id_customer = $request->parameters['customer_uuid'];
        $player_id_driver = $request->parameters['driver_uuid'];
        $bill_trip = $request->parameters['bill_trip'];
        $trip_id = $request->parameters['trip_id'];

        /*$driver_trip = Trips::where('bill_trip', $bill_trip)->pluck('driver_id')->toArray();
        $player_geo_driver = DriverGeo::where('user_id', $driver_trip[0])->pluck('driver_player_id')->toArray();

        $player_id_driver = ($player_geo_driver[0] == $player_id_driver) ? $player_id_driver : $player_geo_driver[0];*/

        //Se cambiara estado del viaje a cancelado
        $log_trip = new TripsLogs();
        $log_trip->trip_id = $trip_id;
        $log_trip->status_trip_id = 6;//es estado CANCELADO
        $log_trip->save();

        //Se inserta en una tabla un registro para los viajes cancelados y rechazados
        $cancelled = new TripsCancelled();
        $cancelled->user_id = $user_id;
        $cancelled->trip_id = $trip_id;
        $cancelled->type = "cancelado";
        $cancelled->comment = isset($comment) ? $comment : "Viaje cancelado por el usuario ".$user_name;
        $cancelled->save();

        $driver_busy = User::where('id', $user_id)
        ->update([
            'busy' => 0,
        ]);

        $content = array(
            "en" => 'El viaje ha sido cancelado. Inténtalo de nuevo', 
        );
    
        $dat= array(
            "factura" => $bill_trip,
            "type" => "2"
        );
    
        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=> array($player_id_customer, $player_id_driver),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content,
            'type' => 'notification/cancelTrip'
        );
        
        \Log::info($fields);
        return $fields;
    }

    public function rejectTrip(Request $request){
        \Log::info("LLEGA A RECHAZAR");
        \Log::info($request);
        $user_id = $request->parameters['user_id'];
        $user_name = $request->parameters['user_name'];
        
        $customer_player_id = $request->parameters['uuid'];
        //$driver_player_id = $request->driver_player_id;
        $bill_trip = $request->parameters['bill_trip'];
        $trip_id = $request->parameters['trip_id'];

        //Se cambiara estado del viaje a RECHAZADO
        $log_trip = new TripsLogs();
        $log_trip->trip_id = $trip_id;
        $log_trip->status_trip_id = 5;//es estado RECHAZADO
        $log_trip->save();

        //Se inserta en una tabla un registro para los viajes cancelados y rechazados
        $cancelled = new TripsCancelled();
        $cancelled->user_id = $user_id;
        $cancelled->trip_id = $trip_id;
        $cancelled->type = "rechazado";
        $cancelled->comment = isset($comment) ? $comment : "Viaje rechazado por el usuario ".$user_name;
        $cancelled->save();

        $content = array(
            "en" => 'Viaje ha sido rechazado', 
        );
    
        $dat= array(
            "factura" => $bill_trip,
            "type" => "5"
        );
    
        $fields = array(
            'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
            'include_player_ids'=>array($customer_player_id),
            'large_icon' =>"ic_launcher_round.png",
            'data' => $dat,
            'contents' => $content,
            'type' => 'notification/rejectTrip'
        );

        \Log::info($fields);

        return $fields;
    }

    public function getTripCustomer(Request $request){
        $arr = [];
        $date = new Carbon();
        $date->subHour();

        \Log::info($date->toDateTimeString());

        try {

            $trip = Trips::addSelect(['last_status' => TripsLogs::select('status_trip_id')
                    ->whereColumn('trip_id',  'trips.id')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
                ])
                ->where('trips.customer_id', $request->id)
                ->where('trips.created_at', '>=', $date->toDateTimeString())
                ->groupBy('trips.bill_trip')
                ->get();

            if($trip && count($trip) > 0){

                foreach ($trip as $key => $value) {
                    if($value['last_status'] < 4){
                        array_push($arr, $trip[$key]);
                    }
                }

                foreach ($arr as $key => $value) {
                    $driver = Driver::join('users', 'users.id', '=', 'driver.user_id')
                            ->join('driver_cars', 'driver_cars.id_driver', '=', 'driver.id')
                            ->where('driver.user_id', $value['driver_id'])
                            ->where('driver_cars.status', 1)
                            ->get(['users.name as name_driver', 'driver.photo', 'driver_cars.model_car', 'driver_cars.brand_car', 'driver_cars.placa', 'driver_cars.color', 'driver.user_id']);

                    $arr[$key]['driver'] = $driver;


                    $driver_car_id = Driver::where('user_id', $value['driver_id'])->pluck('id')->toArray();
                    $car_driver = DriverCars::where('id_driver', $driver_car_id[0])
                            ->where('status', 1)
                            ->get();
                }

                if(!empty($arr)){
                    return response()->json([
                        'data' => array($arr, $car_driver)
                    ], 200);
                }else{
                    return response()->json([
                        'data' => []
                    ], 200);
                }
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function getTripDriver(Request $request){

        try {

            $arr = [];
            $trip = Trips::addSelect(['last_status' => TripsLogs::select('status_trip_id')
                    ->whereColumn('trip_id',  'trips.id')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
                ])
                ->where('driver_id', $request->id)
                ->groupBy('bill_trip')
                ->get();
            
            if($trip && count($trip) > 0){

                foreach ($trip as $key => $value) {
                    if($value['last_status'] < 4){
                        array_push($arr, $trip[$key]);
                    }
                }

                foreach ($arr as $key => $value) {
                    $customer = Customer::join('users', 'users.id', '=', 'customer.user_id')
                        ->where('customer.user_id', $value['customer_id'])
                        ->get(['users.name', 'customer.photo', 'customer.phone']);

                    $arr[$key]['customer'] = $customer;
                }

                return response()->json([
                    'data' => $arr,
                ], 200);
            }else{
                return response()->json([
                    'data' => [],
                ], 200);
            }
        
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function balanceUser(Request $request){
        \Log::info($request);
        //reason_balance, comment, eso ira en la tabla viajes
        $amount = $request->saldo;

        $trip = Trips::find($request->trip_id);
        $trip->balance = $amount;
        $trip->reason_balance = $request->reason_balance;
        $trip->comments = $request->comment;
        $trip->save();

        $balance_user = UserBalance::where('id_user', $request->id_user)->get();
        $balance_data = json_decode(json_encode($balance_user), true);

        if(count($balance_user) == 0){//Significa que el usuario no tiene balances

            if($request->type_balance == 1){//saldo a favor del cliente
                $balance = new UserBalance();
                $balance->positive_balance = $amount;
                $balance->id_user = $request->id_user;
                $balance->comment = isset($request->comment) ? $request->comment : '';
                $balance->save();
            }else{//saldo en contra del cliente
                $balance = new UserBalance();
                $balance->negative_balance = $amount;
                $balance->id_user = $request->id_user;
                $balance->comment = isset($request->comment) ? $request->comment : '';
                $balance->save();
            }

        }else{//significa que ya existe un registro y hay que modificarlo
            if($request->type_balance == 1){//saldo a favor del cliente
                $positive_balance = $balance_data[0]['positive_balance'] + $amount;

                $balance = UserBalance::where('id_user', $request->id_user)
                    ->update(['positive_balance' => $positive_balance, 'comment' => isset($request->comment) ? $request->comment : '']);

            }else{//saldo en contra del cliente
                $negative_balance = $balance_data[0]['negative_balance'] + $amount;

                $balance = UserBalance::where('id_user', $request->id_user)
                    ->update(['negative_balance' => $negative_balance, 'comment' => isset($request->comment) ? $request->comment : '']);
            }
        }

        if($balance){
            return response()->json([
                'data' => $balance,
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No se pudo guardar el balance")
            ], 400);
        }
    }

    public function verifyTripInfo($driver_id){
        try {
            $arr = [];
            $trip = Trips::addSelect(['last_status' => TripsLogs::select('status_trip_id')
                    ->whereColumn('trip_id',  'trips.id')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
                ])
                ->where('driver_id', $driver_id)
                ->orderBy('created_at', 'desc')
                ->limit(1)
                ->get();

            if($trip && count($trip) > 0){

                foreach ($trip as $key => $value) {
                    if($value['last_status'] < 4){
                        array_push($arr, $trip[$key]);
                    }
                }

                if(count($arr) > 0){
                    return $arr;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        
        } catch (\Exception $e) {
            return 0;
        }
    }
}
