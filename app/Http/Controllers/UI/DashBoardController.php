<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;
use View;
use JWTAuth;
use Session;
use App\User;
use App\Driver;
use App\DriverCars;
use App\Customer;
use App\UserRoles;
use App\Trips;
use App\TripsLogs;
use Carbon\Carbon;

class DashBoardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDataDashboard(){
        $arr_active = [];
        $arr_finish = [];

        \Log::info(Carbon::today());
        $now = Carbon::now();
        $trips_day = Trips::whereDate('created_at', Carbon::today())->count();
        
        $payment_day = Trips::whereDate('created_at', Carbon::today())
            ->sum('cost');

        $payment_month = Trips::whereMonth('created_at', Carbon::now()->month)
            ->sum('cost');
        
        $payment_cash_month = Trips::whereMonth('created_at', Carbon::now()->month)
            ->where('payment_id', 1)
            ->sum('cost');

        $payment_card_month = Trips::whereMonth('created_at', Carbon::now()->month)
            ->where('payment_id', 2)
            ->sum('cost');
        
        $customer_active = User::join('customer', 'customer.user_id', '=', 'users.id')
            ->join('user_roles', 'user_roles.id_user', '=', 'users.id')
            ->where('user_roles.id_role', 2)
            ->where('users.status', 1)
            ->count();

        $driver_active = User::join('driver', 'driver.user_id', '=', 'users.id')
            ->join('user_roles', 'user_roles.id_user', '=', 'users.id')
            ->where('user_roles.id_role', 3)
            ->where('users.status', 1)
            ->count();
        
        $trips_active = Trips::addSelect(['last_status' => TripsLogs::select('status_trip_id')
                ->whereColumn('trip_id',  'trips.id')
                ->orderBy('created_at', 'desc')
                ->limit(1)
            ])
            ->addSelect(['customer_name' => User::select('name')
                ->whereColumn('id',  'trips.customer_id')
            ])
            ->addSelect(['driver_name' => User::select('name')
                ->whereColumn('id',  'trips.driver_id')
            ])
            ->whereDate('created_at', Carbon::today())
            ->orderBy('created_at', 'desc')
            ->groupBy('trips.bill_trip')
            ->take(5)
            ->get();

        $driver_trips = Trips::addSelect(['driver_name' => User::select('name')
                ->whereColumn('id',  'trips.driver_id')
            ])
            ->selectRaw("SUM(cost) as total_costo")
            ->selectRaw("count(*) as total_trips")
            ->whereDate('created_at', Carbon::today())
            ->orderBy('created_at', 'desc')
            ->groupBy('trips.driver_id')
            ->take(5)
            ->get();
        
        if($trips_active && count($trips_active) > 0){
            foreach ($trips_active as $key => $value) {
                if($value['last_status'] < 4){
                    array_push($arr_active, $trips_active[$key]);
                }elseif ($value['last_status'] == 4) {
                    array_push($arr_finish, $trips_active[$key]);
                }
            }
        }else{
           $arr_active = [];
           $arr_finish = [];
        }

        return view("dashboard", [
            "trips_day" => $trips_day, 
            "payment_day" => round($payment_day, 2),
            "customer_active" => $customer_active, 
            "driver_active" => $driver_active, 
            "arr_active" => $arr_active, 
            "arr_finish" => $arr_finish, 
            "payment_month" => $payment_month, 
            "payment_cash_month" => round($payment_cash_month, 2), 
            "payment_card_month" => round($payment_card_month, 2),
            "driver_trips" => $driver_trips
        ]);

    }
}
