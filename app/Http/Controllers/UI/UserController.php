<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Hash;
use View;
use Crypt;
use Session;
use JWTAuth;
use App\User;
use App\Driver;
use App\Customer;
use App\UserRoles;
use App\DriverGeo;
use App\DriverCars;
use App\SuspendedUser;
use Illuminate\Support\Str;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->mail_controller = new MailController;
    }

    public function getUpdatedTable() {
        $drivers = User::join('driver', 'driver.user_id', '=', 'users.id')
                ->join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->where('user_roles.id_role', 3)
                ->select('users.id', 'users.password', 'users.name', 'users.email', 'users.status','driver.phone', 'driver.photo', 'driver.photo_dui', 'driver.photo_licence', 'driver.photo_background', 'driver.photo_solvency_pnc')
                ->orderBy('users.id', 'desc')
                ->paginate(15);

        return view("users.driver", [ "drivers", $drivers ]);
    } 

    public function getDrivers(Request $request){

        $drivers = User::join('driver', 'driver.user_id', '=', 'users.id')
                ->join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->where('user_roles.id_role', 3)
                //->where('users.status', 1)
                ->where('users.suspended', 0)
                ->select('users.id', 'users.password', 'users.name', 'users.email', 'users.status','driver.phone', 'driver.photo', 'driver.photo_dui', 'driver.photo_licence', 'driver.photo_background', 'driver.photo_solvency_pnc')
                ->orderBy('users.id', 'desc')
                ->paginate(15);

        return View::make('users.driver')->with('drivers', $drivers);
    }

    public function searchDriver(Request $request){
        $drivers = User::join('driver', 'driver.user_id', '=', 'users.id')
                ->join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->where('user_roles.id_role', 3)
                ->where('users.name', 'like', '%' . $request->driver_search . '%')
                ->select('users.id','users.name', 'users.password', 'users.email', 'users.status','driver.phone', 'driver.photo', 'driver.photo_dui', 'driver.photo_licence', 'driver.photo_background', 'driver.photo_solvency_pnc')
                ->orderBy('users.id', 'desc')
                ->paginate(15);
        
        return View::make('users.driver')->with('drivers', $drivers);
    }

    public function getCustomers(Request $request){
        $customers = User::join('customer', 'customer.user_id', '=', 'users.id')
                ->join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->where('user_roles.id_role', 2)
                //->where('users.status', 1)
                ->where('users.suspended', 0)
                ->select('users.id','users.name', 'users.password', 'users.email', 'users.status','customer.phone', 'customer.photo', 'customer.dui')
                ->orderBy('users.id', 'desc')
                ->paginate(15);
        
        return View::make('users.customer')->with('customers', $customers);
    }

    public function searchCustomer(Request $request){
        \Log::info($request);
        $customers = User::join('customer', 'customer.user_id', '=', 'users.id')
                ->join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->where('user_roles.id_role', 2)
                //->where('users.name', 'like', "%{$request->customer_search}%")
                ->where('users.name', 'LIKE','%' . $request->customer_search . '%')
                ->select('users.id','users.name', 'users.password', 'users.email', 'users.status','customer.phone', 'customer.photo', 'customer.dui')
                ->orderBy('users.id', 'desc')
                ->paginate(15);
        
        return View::make('users.customer')->with('customers', $customers);
    }

    public function getAdmins(Request $request){

        $admins = User::join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->where('user_roles.id_role', 1)
                ->select('users.id', 'users.name', 'users.email', 'users.status', 'profile_url')
                ->orderBy('users.id', 'desc')
                ->get();

        \Log::info($admins);

        return View::make('users.admin')->with('admins', $admins);
    }

    public function getSuspendedUser(Request $request){
        $users = SuspendedUser::join('users', 'users.id', '=', 'suspended_users.user_id') 
                ->join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->where('users.suspended', 1)
                ->select('suspended_users.user_id', 'suspended_users.reason', 'suspended_users.comment', 'users.name', 'users.email', 'users.status', 'users.suspended', 'user_roles.id_role')
                ->orderBy('users.id', 'desc')
                ->paginate(15);

        \Log::info($users);
        return View::make('users.suspended')->with('users', $users);
    }

    public function registerAdmin(Request $request){
        \Log::info($request);

        $exist_email = User::where('email', $request->email)->get()->count();

        if($exist_email == 0){

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|regex:/(.*)@aivoiapp.com/i|unique:users'
            ]);

            if($validator->fails()){
                Session::flash('error', $validator->messages()->first());
                return redirect()->back()->withInput()->withErrors($validator);
            }

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->api_token = Str::random(40);
            $user->password = bcrypt($request->password);
            $user->status = 1;
            $user->save();

            if ($user) {
                //roles 1-admin; 2-cliente; 3-driver

                $user_role = new UserRoles();
                $user_role->id_user = $user->id;
                $user_role->id_role = 1;
                $user_role->save();

                return back()->with('success','Admin creado correctamente');
            }
        
        }else{
            return back()->with('error','Ya existe un usuario con el correo brindado');
        }
    }

    public function activateDriver(Request $request){

        $id = $request->id;
        $name_driver = $request->name_driver;
        $email_driver = $request->email_driver;

        $user = User::where('id', $id)
            ->update([
                'status' => 1,
            ]);

        if($user){

            $request_data = new Request([
                'name_driver' => $name_driver,
                'email' => $email_driver,
                'url' => 'emails.account',
            ]);

            $this->mail_controller->confirmAccountDriver($request_data);

            return response()->json([
                'success' => true,
                'data' => "El driver fue activado"
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo activar el driver"
            ], 400);
        }
    }

    public function desactivateDriver(Request $request){

        $id = $request->id;

        $user = User::where('id', $id)
                ->update([
                    'status' => 0,
                ]);

        if($user){
            return response()->json([
                'success' => true,
                'data' => "El driver fue desactivado"
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo desactivar el driver"
            ], 400);
        }
    }

    public function deleteAdmin(Request $request){

        $id = $request->id;

        $user = User::find($id)->delete();

        if($user){
            return response()->json([
                'success' => true,
                'data' => "El administrador fue eliminado"
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo eliminar el administrador"
            ], 400);
        }
    }

    public function desactivateCustomer(Request $request){
        $id = $request->id;

        $user = User::where('id', $id)
            ->update([
                'status' => 0,
            ]);

        if($user){
            return response()->json([
                'success' => true,
                'data' => "El usuario fue desactivado"
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo desactivar el usuario"
            ], 400);
        }
    }

    public function activateCustomer(Request $request){
        $id = $request->id;

        $user = User::where('id', $id)
            ->update([
                'status' => 1,
            ]);

        if($user){
            return response()->json([
                'success' => true,
                'data' => "El usuario fue activado"
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo activar el usuario"
            ], 400);
        }
    }

    public function suspendUser(Request $request){
        \Log::info($request);
        //$user = JWTAuth::parseToken()->authenticate();
        $user = JWTAuth::user();
        $auth_id = $user->id;

        $user_id = $request->user_id;
        $reason = $request->reason;
        $comment = $request->comment;
        $suspended_by = $auth_id;

        $user = User::where('id', $user_id)
                ->update([
                    'suspended' => 1,
                ]);

        $suspended = DB::table('suspended_users')
            ->insert([
                'user_id' => $user_id,
                'reason' => $reason,
                'comment' => $comment,
                'suspended_by' => $suspended_by
            ]);

        if($user && $suspended){
            // return back()->with('success','El usuario fue suspendido');
            return response()->json([
                'success' => true,
                'data' => "El usuario fue suspendido"
            ], 200);
        }else{
            // return back()->with('error','No se pudo suspender el usuario');
            return response()->json([
                'error' => true,
                'data' => "No se pudo suspender el usuario"
            ], 400);
        }
    }

    public function changePwd(Request $request){
        \Log::info($request);
        $user = User::where('id', '=', $request->user_id)->first();

        if(isset($user)){
            //Si existe entonces hare un random de la nueva contraseña y se manda por email
            $new_pwd = $request->password;

            $user->password = bcrypt($new_pwd);
            $user->save();

            $user_email = User::where('id', $request->user_id)->pluck('email')->toArray();

            DB::table('reset_pwd_log')->insert([
                'email' => $user_email[0],
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            return response()->json([
                'success' => true,
                'data' => 'Contraseña cambiada con éxito'
            ], 200);

        }else{
            return response()->json([
                'error' => true,
                'data' => 'No se pudo cambiar la contraseña'
            ], 400);
        }
    }

    public function revalidateUser(Request $request){
        \Log::info($request);

        $user_id = $request->user_id;

        $user = User::where('id', $user_id)
            ->update([
                'suspended' => 0,
            ]);

        if($user){
            return response()->json([
                'success' => true,
                'data' => "El usuario fue revalidado"
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo revalidar el usuario"
            ], 400);
        }
    }

    public function sendNotificationDriver(Request $request){
        \Log::info($request);
        $user_id = $request->user_id;
        $message = $request->message;
        $name_driver = $request->name_driver;

        $driver_player_id = DriverGeo::where('user_id', $user_id)->pluck('driver_player_id')->toArray();

        if(isset($driver_player_id[0])){
            $content = array(
                "en" => $message, 
            );
        
            $dat= array(
                "Driver" => $name_driver
            );

            $fields = array(
                'app_id' => "4524e909-1cf0-4a21-94e4-9e8aa0cd3560",
                'include_player_ids'=>array($driver_player_id[0]),
                'large_icon' =>"ic_launcher_round.png",
                'data' => $dat,
                'contents' => $content
            );
    
            $fields = json_encode($fields);
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                       'Authorization: Basic ODk3MmFiMTctYWEyMi00ODFmLTgwM2QtZTUzMmQzYjQ3NDE1'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
        
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
    
            if ($err) {
                return response()->json([
                    'error' => true,
                    'data' => $err
                ], 400);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "Sent",
                    'allresponses' => $response,
                ], 200);
            }
        }else{
            return response()->json([
                'error' => true,
                'data' => "El driver no tiene activa la app en este momento"
            ], 400);
        }
    }

    public function changeAdminPhoto(Request $request){
        \Log::info($request);
        if($request->photo){
            $imagedata = file_get_contents($request->file('photo'));
            $base64 = base64_encode($imagedata);

            $user = User::find($request->user_id);
            $user->profile_url = $base64;
            $user->save();

            if($user){
                return response()->json([
                    'success' => true,
                    'data' => "Imagen cambiada",
                ], 200);
            }else{
                return response()->json([
                    'error' => true,
                    'data' => "No se pudo guardar la imagen"
                ], 400);
            }
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se encontró la imagen"
            ], 400);
        }
    }

    public function verifyNewUsers(Request $request){
        $drivers = User::join('user_roles', 'user_roles.id_user', '=', 'users.id')
            ->where('user_roles.id_role', '<>', 1)
            ->where('status', 0)
            ->get();

        $count = $drivers->count();

        return response()->json([
            'data' => $count
        ]);
    }
}
