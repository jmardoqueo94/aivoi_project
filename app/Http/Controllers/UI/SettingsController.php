<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Hash;
use View;
use Crypt;
use Session;
use JWTAuth;
use App\PriceVariants;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    public function index(){
        $settings = PriceVariants::get();
        \Log::info($settings);

        return View::make('settings.index')->with('settings', $settings);
    }

    public function editSettings(Request $request){

        \Log::info($request);

        $id = $request->id;
        $setting = PriceVariants::find($id);
        $setting->driver = $request->driver;
        $setting->traffic = $request->traffic;
        $setting->short_distance = $request->short_distance;
        $setting->long_distance = $request->long_distance;
        $setting->min = $request->min;
        $setting->base = $request->base;
        $setting->iva = $request->iva;
        $setting->credit_card = $request->credit_card;
        $setting->save();

        if($setting){
            return response()->json([
                'success' => true,
                'data' => "La comisión fue cambiada."
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo editar la camisión"
            ], 400);
        }

    }
}
