<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;
use View;
use JWTAuth;
use Session;
use App\User;
use App\Driver;
use App\DriverCars;
use App\Customer;
use App\UserRoles;
use App\Trips;
use App\TripsLogs;
use App\Payment;
use Carbon\Carbon;

class ReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTrips(Request $request){
        $now = Carbon::now();
        $trips = Trips::addSelect(['customer_name' => User::select('name')
            ->whereColumn('id',  'trips.customer_id')
        ])
        ->addSelect(['driver_name' => User::select('name')
            ->whereColumn('id',  'trips.driver_id')
        ])
        ->addSelect(['payment' => Payment::select('type')
            ->whereColumn('id',  'trips.payment_id')
        ])
        ->addSelect(['last_status' => TripsLogs::select('status_trip_id')
            ->whereColumn('trip_id',  'trips.id')
            ->orderBy('created_at', 'desc')
            ->limit(1)
        ])
        ->whereDate('created_at', Carbon::today())
        ->paginate(25);
        
        return View::make('reports.trips_customer')->with('trips', $trips);
    }

    public function getTripsDriver(Request $request){

        $trips = Trips::addSelect(['customer_name' => User::select('name')
            ->whereColumn('id',  'trips.customer_id')
        ])
        ->addSelect(['driver_name' => User::select('name')
            ->whereColumn('id',  'trips.driver_id')
        ])
        ->addSelect(['payment' => Payment::select('type')
            ->whereColumn('id',  'trips.payment_id')
        ])
        ->addSelect(['last_status' => TripsLogs::select('status_trip_id')
            ->whereColumn('trip_id',  'trips.id')
            ->orderBy('created_at', 'desc')
            ->limit(1)
        ])
        ->whereDate('created_at', Carbon::today())
        ->paginate(25);
        
        return View::make('reports.trips_driver')->with('trips', $trips);

    }

    public function filteredDateTrips(Request $request){
        \Log::info($request);
        if($request->filter == "day"){
            $date = Carbon::today();
        }elseif ($request->filter == "week") {
            $date = Carbon::today()->subDays(7);
        }else{
            $date = Carbon::today()->subDays(30);
        }

        \Log::info($date);
        
        $trips = Trips::addSelect(['customer_name' => User::select('name')
            ->whereColumn('id',  'trips.customer_id')
        ])
        ->addSelect(['driver_name' => User::select('name')
            ->whereColumn('id',  'trips.driver_id')
        ])
        ->addSelect(['payment' => Payment::select('type')
            ->whereColumn('id',  'trips.payment_id')
        ])
        ->addSelect(['last_status' => TripsLogs::select('status_trip_id')
            ->whereColumn('trip_id',  'trips.id')
            ->orderBy('created_at', 'desc')
            ->limit(1)
        ])
        ->whereDate('created_at','>=', $date)->paginate(25);

        $trips = json_decode(json_encode($trips), true);
        
        if($trips){
            return response()->json([
                'success' => true,
                'data' => $trips
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => []
            ], 400);
        }
    }

    //RUTAS PARA MOSTRAR VIAJES DE CLIENTES
    public function searchCustomerTrips(Request $request){
        
        $trips = Trips::join('users', 'users.id', '=', 'trips.customer_id')
            ->join('customer', 'customer.user_id', '=', 'users.id')
            ->addSelect(['driver_name' => User::select('name')
                ->whereColumn('id',  'trips.driver_id')
            ])
            ->addSelect(['payment' => Payment::select('type')
                ->whereColumn('id',  'trips.payment_id')
            ])
            ->addSelect(['customer_name' => User::select('name')
                ->whereColumn('id',  'trips.customer_id')
            ])
            ->addSelect(['last_status' => TripsLogs::select('status_trip_id')
                ->whereColumn('trip_id',  'trips.id')
                ->orderBy('created_at', 'desc')
                ->limit(1)
            ])
            ->where("users.name", "LIKE", "%{$request->search}%")
            //->select('trips.bill_trip', 'trips.cost', 'users.id', 'users.email', 'users.status','customer.phone', 'customer.photo', 'customer.dui', 'driver_name', 'payment', 'customer_name', 'last_status')
            ->orderBy('trips.created_at', 'desc')
            ->paginate(15);

        \Log::info($trips);

        if($trips){
            return response()->json([
                'success' => true,
                'data' => $trips
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => []
            ], 400);
        }
    }

    //PARA REPORTES DE DRIVERS MOSTRAR VIAJES SOLO DE UN DRIVER Y LO QUE DEBE
    public function searchDriverTrips(Request $request){
        \Log::info($request);
        
        $trips = Trips::join('users', 'users.id', '=', 'trips.driver_id')
            ->join('driver', 'driver.user_id', '=', 'trips.driver_id')
            ->addSelect(['driver_name' => User::select('name')
                ->whereColumn('id',  'trips.driver_id')
            ])
            ->addSelect(['payment' => Payment::select('type')
                ->whereColumn('id',  'trips.payment_id')
            ])
            ->addSelect(['customer_name' => User::select('name')
                ->whereColumn('id',  'trips.customer_id')
            ])
            ->addSelect(['last_status' => TripsLogs::select('status_trip_id')
                ->whereColumn('trip_id',  'trips.id')
                ->orderBy('created_at', 'desc')
                ->limit(1)
            ])
            ->where("users.name", "LIKE", "%{$request->search}%")
            ->orderBy('trips.created_at', 'desc')
            ->paginate(25);

            \Log::info($trips);

        if($trips){
            return response()->json([
                'success' => true,
                'data' => $trips
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => []
            ], 400);
        }

    }
}
