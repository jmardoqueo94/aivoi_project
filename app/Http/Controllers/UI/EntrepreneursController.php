<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Hash;
use View;
use Crypt;
use Session;
use JWTAuth;
use App\Entrepreneurs;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;

class EntrepreneursController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $emprendedor = Entrepreneurs::get();

        return View::make('entrepreneurs.index')->with('emprendedor', $emprendedor);
    }

    public function register(Request $request){
        $exist_email = Entrepreneurs::where('email', $request->email)->get()->count();

        if($exist_email == 0){

            $validator = Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            if($validator->fails()){
                Session::flash('error', $validator->messages()->first());
                return redirect()->back()->withInput()->withErrors($validator);
            }

            $emp = new Entrepreneurs();
            $emp->name = $request->name;
            $emp->phone = $request->phone;
            $emp->email = $request->email;
            $emp->address = $request->address;
            $emp->contact_name = $request->contact_name;
            $emp->save();

            if ($emp) {
                return back()->with('success','Emprendedor creado correctamente');
            }
        
        }else{
            return back()->with('error','Ya existe un emprendedor con el correo brindado');
        }
    }

    public function editEmprendedor(Request $request){

        \Log::info($request);

        $id = $request->id;
        $emp = Entrepreneurs::find($id);
        $emp->name = $request->name;
        $emp->phone = $request->phone;
        $emp->email = $request->email;
        $emp->address = $request->address;
        $emp->contact_name = $request->contact_name;
        $emp->save();

        if($emp){
            return response()->json([
                'success' => true,
                'data' => "Emprendedor editado correctamente."
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo editar el emprendedor"
            ], 400);
        }

    }

    public function deleteEmprendedor(Request $request){

        $id = $request->id;

        $user = Entrepreneurs::find($id)->delete();

        if($user){
            return response()->json([
                'success' => true,
                'data' => "El emprendedor fue eliminado"
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => "No se pudo eliminar el emprendedor"
            ], 400);
        }
    }
}
