<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use JWTAuth;
use Session;
use App\User;
use App\Trips;
use App\Driver;
use App\Payment;
use App\Customer;
use App\UserRoles;
use App\TripsLogs;
use App\DriverGeo;
use Carbon\Carbon;
use App\DriverCars;

class TrackingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function mapTracking(){
        return View::make('tracking.track_driver');
    }

    public function getCoords(Request $request){
        $new_array = [];
        $coords = DriverGeo::join('users', 'users.id', '=', 'driver_geo.user_id')
            ->join('driver', 'driver.user_id', '=', 'users.id')
            ->where('users.status', 1)
            ->where('users.online', 1)
            ->select('users.name', 'users.email', 'users.busy','driver_geo.lat', 'driver_geo.lng', 'driver.phone')
            ->get();

        foreach ($coords as $key => $value) {
            $new_array[] = array(
                "type" => "FeatureCollection",
                "features" => array(
                    array(
                        "type" => "Feature",
                        "geometry" => array(
                            "type" => "Point",
                            "coordinates" => array(
                                $value->lng, $value->lat
                            )
                        ),
                        "properties" => array(
                            "title" => "Driver",
                            "description" => $value->name,
                            "status" => "online",
                            "phone" => $value->phone,
                            "busy" => $value->busy
                        )
                    )
                )
            );
        }

        $new_array = json_decode(json_encode($new_array), true);

        if(!empty($new_array)){
            return response()->json([
                'success' => true,
                'data' => $new_array
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'data' => []
            ], 400);
        }

    }

}
