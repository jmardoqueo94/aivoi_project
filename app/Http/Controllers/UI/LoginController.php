<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use Hash;
use View;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function loginUI(Request $request){
        $request->validate([
            "email"           =>    "required|email",
            "password"        =>    "required|min:6"
        ]);

        $userCredentials = $request->only('email', 'password');

        // check user using auth function
        if (Auth::attempt($userCredentials)) {
            return redirect()->intended('/');
        }

        else {
            return back()->with('error', 'Whoops! email o password incorrecto!');
        }
    }
}
