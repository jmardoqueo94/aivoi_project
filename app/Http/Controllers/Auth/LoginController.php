<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
//#Daiki1324#

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public $loginAfterSignUp = true;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

     public function login(Request $request)
    {
        \Log::info("Esto llega al login");
        \Log::info($request);
        $input = $request->only('email', 'password');
        $token = null;

        $user = User::where('email', '=', $input['email'])->where('status', '=', 0)->get();

        if (count($user) > 0) {
            return response()->json([
                'data' => array('message' => "Usuario desactivado, contacte al administrador.")
            ], 400);
        }

        $user_find = User::where('email', '=', $input['email'])->get();
        \Log::info($user_find);
        if (count($user_find) == 0) {
            return response()->json([
                'data' => array('message' => "No existe ningún usuario registrado con esos datos")
            ], 400);
        }

        if (!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'data' => array('message' => "Correo o contraseña inválida")
            ], 400);
        }

        if($request['role'] == 2){
            $user_data = User::join('customer', 'customer.user_id', '=', 'users.id')
                ->where('users.email', '=', $input['email'])
                ->where('users.status', '=', 1)
                ->get();
        }elseif ($request['role'] == 3) {
            $user_data = User::join('driver', 'driver.user_id', '=', 'users.id')
                ->where('users.email', '=', $input['email'])
                ->where('users.status', '=', 1)
                ->get();
            
            \Log::info('user data');
            foreach ($user_data as $key => $value) {
                if($value['bank_id'] == null){
                    $user_data[0]['data_bank_complete'] = 0;
                }else{
                    $user_data[0]['data_bank_complete'] = 1;
                }
            }
        }

        if(isset($user_data) && count($user_data) > 0){
            return response()->json([
                'data' => array('user' => $user_data[0], 'token' => $token),
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No puede iniciar sesión con el usuario, contacte al administrador")
            ], 400);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {

            $user = JWTAuth::toUser($request->token);
            $user = json_decode(json_encode($user), true);

            $online = User::where('id', $user['id'])
                ->update([
                    'online' => 0, 
                ]);

            JWTAuth::invalidate($request->token);

            return response()->json([], 200);
        } catch (JWTException $exception) {
            return response()->json([
                'data' => array('message' => 'Error al cerrar sesión')
            ], 500);
        }
    }
}
