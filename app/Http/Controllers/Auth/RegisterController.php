<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\UserRoles;
use App\Customer;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use JWTAuth;
use Illuminate\Support\Str;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\CustomerController;
use App\Mail\TestEmail;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->driver_controller = new DriverController;
        $this->customer_controller = new CustomerController;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request)
    {
        $exist_email = User::where('email', $request->email)->get()->count();

        if($exist_email == 0){
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->tag = isset($request->tag) ? $request->tag : null;
            $user->api_token = Str::random(40);
            $user->password = bcrypt($request->password);
            $user->status = 0;
            $user->testing = isset($request->testing) ? $request->testing : 0;
            $user->save();

            if ($user) {
                //roles 1-admin; 2-cliente; 3-driver
                $user_role = new UserRoles();
                $user_role->id_user = $user->id;
                $user_role->id_role = $request->role;
                $user_role->save();

                if($request->role == 2){

                    $request_customer = new Request([
                        'user_id'   => $user->id,
                        //'info_device' => $request->info_device,
                        'phone'  => $request->phone,
                        'photo'  => isset($request->photo) ? $request->photo : null,
                        'name' => $request->name,
                        'email' => $request->email
                    ]);

                    $save_customer = $this->customer_controller->saveCustomer($request_customer);

                    if($save_customer != 'error'){
                        $user_data = User::join('customer', 'customer.user_id', '=', 'users.id')
                            ->where('users.email', '=', $request->email)
                            ->where('users.status', '=', 1)
                            ->get();

                        $token = JWTAuth::fromUser($user);

                        return response()->json([
                            'data' => array('user' => $user_data[0], 'token' => $token),
                        ], 200);
                    }else{
                        return response()->json([
                            'data' => array('message' => "No se pudo crear el cliente")
                        ], 400);
                    }

                }elseif($request->role == 3){
                    $request_driver = new Request([
                        'user_id'   => $user->id,
                        'name' => $request->name,
                        'email' => $request->email,
                        'phone'  => $request->phone,
                        'photo'  => isset($request->photo) ? $request->photo : null,
                        'photo_dui'  => isset($request->photo_dui) ? $request->photo_dui : null,
                        'photo_licence'  => isset($request->photo_licence) ? $request->photo_licence : null,
                        'photo_background'  => isset($request->photo_background) ? $request->photo_background : null,
                        'photo_solvency'  => isset($request->photo_solvency) ? $request->photo_solvency : null,
                    ]);
                    
                    $save_driver = $this->driver_controller->saveDriver($request_driver);

                    if($save_driver != 'error'){
                        return response()->json([
                            'data' => $save_driver,
                        ], 200);
                    }else{
                        return response()->json([
                            'data' => array('message' =>"No se pudo crear el driver")
                        ], 400);
                    }
                }else{
                    return response()->json([
                        'data' => $user,
                    ], 200);
                }
            }else{
                return response()->json([
                    'data' => array('message' => "No se pudo crear el usuario")
                ], 400);
            }
        }else{
            return response()->json([
                'data' => array('message' => "Ya existe un usuario con el correo brindado")
            ], 400);
        }
    }
}
