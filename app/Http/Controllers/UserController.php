<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use JWTAuth;
use App\User;
use App\RatingUser;
use App\UserBalance;
use App\Mail\TestEmail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\MailController;
use Tymon\JWTAuth\Exceptions\JWTException;


class UserController extends Controller
{
    public function __construct()
    {
        $this->mail_controller = new MailController;
    }

    public function getCustomers(Request $request){
        //customer es id 2
        try {
            $users = User::join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->join('customer', 'customer.user_id', '=', 'users.id')
                ->where('users.status', 1)
                ->where('user_roles.id_role', 2)
                ->get();
                
            if ($users) {
                return response()->json([
                    'data' => $users
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function getDrivers(Request $request){
        //driver es id 3
        try {
            $drivers = User::join('user_roles', 'user_roles.id_user', '=', 'users.id')
                    ->join('driver', 'driver.user_id', '=', 'users.id')
                    ->where('users.status', 1)
                    ->where('user_roles.id_role', 3)
                    ->get();

            if ($drivers) {
                return response()->json([
                    'data' => $drivers
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function getCustomerData(Request $request){

        try {

            $id = $request->customer_id;
        
            $user = User::join('customer', 'customer.user_id', '=', 'users.id')
                    ->join('balance_users', 'balance_users.id_user', '=', 'users.id')
                    ->where('users.status', 1)
                    ->where('users.id', $id)
                    ->get(['users.*', 'balance_users.positive_balance', 'balance_users.negative_balance']);

            /*$balance = UserBalance::where('id_user', $id)->select('positive_balance', 'negative_balance')->get();
            $user = json_decode(json_encode($user), true);
            $new_user = array_values($user);
            foreach($balance as $row){
                $new_user['positive_balance'] = $row->positive_balance;
                $new_user['negative_balance'] = $row->negative_balance;
            }*/
            
            if ($user) {
                return response()->json([
                    'data' => $user
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
            
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }        
    }

    public function getDriverData(Request $request){
        try {

            $id = $request->driver_id;
        
            $user = User::join('driver', 'driver.user_id', '=', 'users.id')
                    ->where('users.status', 1)
                    ->where('users.id', $id)
                    ->get();
        
            if ($user) {

                foreach ($user as $key => $value) {
                    if($value['bank_id'] == null){
                        $user[0]['data_bank_complete'] = 0;
                    }else{
                        $user[0]['data_bank_complete'] = 1;
                    }
                }

                return response()->json([
                    'data' => $user
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
            
        } catch (\Exception $e) {
            return response()->json([
                'data' => array('message' => $e->getMessage())
            ], 400);
        }
    }

    public function desactivateUser(Request $request){
        
        //$user = JWTAuth::parseToken()->authenticate();
        $id = $request->user_id;

        $desactivate = User::find($id);
        $desactivate->status = 0;
        $desactivate->save();

        if ($desactivate) {
            return response()->json([
                'data' => array('message' => 'Usuario desactivado')
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "No se pudo desactivar el usuario")
            ], 400);
        }
    }

    public function changePassword(Request $request){

        $user = JWTAuth::parseToken()->authenticate();

        $crip_pass = bcrypt($user->password);

        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json([
                'data' => array('message' => "La contraseña actual no coincide")
            ], 400);
        }

        $user->password = bcrypt($request->new_password);
        $user->save();

        return response()->json([
            'data' => array('message' => 'Contraseña cambiada exitosamente')
        ], 200);
    }

    public function resetPassword(Request $request){
        \Log::info("ESTO VIENE EN RESTORE");
        \Log::info($request);

        $user = User::where('email', '=', $request->email)->first();

        if(isset($user)){
            //Si existe entonces hare un random de la nueva contraseña y se manda por email
            $pwd_tmp = Str::random(8);

            $user->password = bcrypt($pwd_tmp);
            $user->save();

            DB::table('reset_pwd_log')->insert([
                'email' => $request->email,
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            $request_data = new Request([
                'password' => $pwd_tmp,
                'email' => $request->email,
                'name_customer' => $user['name'],
                'url' => 'emails.restore',
            ]);

            $this->mail_controller->restorePassword($request_data);

            return response()->json([
                'data' => array('message' => 'Contraseña temporal enviada con éxito')
            ], 200);

        }else{
            return response()->json([
                'data' => array('message' => 'No existe ninguna cuenta con ese correo')
            ], 400);
        }
    }

    public function verifyPassword(Request $request){

        $user = JWTAuth::parseToken()->authenticate();

        $crip_pass = bcrypt($user->password);

        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json([
                'data' => array('message' => "La contraseña actual no coincide")
            ], 400);
        }else{
            return response()->json([
                'data' => array('message' => "Las contraseñas coinciden")
            ], 200);
        }
    }

    public function calificateUser(Request $request){

        $rating = new RatingUser;
        $rating->score = $request->score;
        $rating->id_user = $request->id_user;
        $rating->comment = isset($request->comment) ? $request->comment : '' ;
        $rating->save();

        if($rating){
            return response()->json([
                'data' => array('message' => 'Calificacion hecha exitosamente')
            ], 200);
        }else{
            return response()->json([
                'data' => array('message' => "Calificación no se pudo guardar")
            ], 400);
        }
    }

    public function getUserData(Request $request){
        \Log::info("GET USER DATA");
        \Log::info($request);

        $user_id = $request->user_id;

        $user = User::join('user_roles', 'user_roles.id_user', '=', 'users.id')
                ->where('user_roles.id_user', $user_id)
                ->pluck('user_roles.id_role')
                ->toArray();

        if($user){
            if($user[0] == 2){

                $user_data = User::join('customer', 'customer.user_id', '=', 'users.id')
                    ->where('users.status', 1)
                    ->where('users.id', $user_id)
                    ->get();
    
            }elseif ($user[0] == 3) {
                
                $user_data = User::join('driver', 'driver.user_id', '=', 'users.id')
                    ->where('users.status', 1)
                    ->where('users.id', $user_id)
                    ->get();
    
                foreach ($user_data as $key => $value) {
                    if($value['bank_id'] == null){
                        $user_data[0]['data_bank_complete'] = 0;
                    }else{
                        $user_data[0]['data_bank_complete'] = 1;
                    }
                }
            }
            
            if(count($user_data) > 0){
    
                return response()->json([
                    'data' => $user_data[0]
                ], 200);
            }else{
                return response()->json([
                    'data' => []
                ], 200);
            }
        }else{
            return response()->json([
                'data' => array('message' => 'No hay datos')
            ], 400);
        }        
    }
}
