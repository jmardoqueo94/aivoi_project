<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\User;
use App\Driver;
use App\Invoice;
use App\Customer;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\MetricsController;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->metrics_controller = new MetricsController;
    }

    public function createInvoice(Request $request){

        $driver_name = User::where('email', $request->email_driver)->pluck('name')->toArray();
        $customer_name = User::where('email', $request->email_customer)->pluck('name')->toArray();
        $bill = Str::random(10);

        $invoice = array(
            "bill_trip" => $bill,
            "origin_lat" => $request->initial_lat,
            "origin_lng" => $request->initial_lng,
            "destination_lat" => $request->finish_lat,
            "destination_lng" => $request->finish_lng,
            "price" => $request->price,
            "name_driver" => $driver_name[0],
            "name_customer" => $customer_name[0]
        );

        $invoice = new Invoice();
        $invoice->bill_trip = $bill;
        $invoice->origin_lat = $request->initial_lat;
        $invoice->origin_lng = $request->initial_lng;
        $invoice->destination_lat = $request->finish_lat;
        $invoice->destination_lng = $request->finish_lng;
        $invoice->price = $request->price;
        $invoice->driver_id = $request->driver_id;
        $invoice->name_driver = $driver_name[0];
        $invoice->name_customer = $customer_name[0];
        $invoice->email_customer = $request->email_customer;
        $invoice->email_driver = $request->email_driver;
        $invoice->save();

        if($invoice){
            return response()->json([
                'data' => $invoice,
            ], 200);
        }else{
            return response()->json([
                'data' => []
            ], 400);
        }
    }

    public function setBalanceDriver(Request $request){
        $date = Carbon::today();
        $driver_id = $request->driver_id;

        $trips = Invoice::select("origin_lat", "origin_lng", "destination_lat", "destination_lng", "price")
            ->where('driver_id', $driver_id)->whereDate('created_at','>=', $date)
            ->groupBy('id')
            ->get();

        $sum_trips = count($trips);
        
        if($sum_trips > 0){
            $sum_cost = 0;
            $sum_distance = 0;

            foreach ($trips as $key => $value) {
                $km = $this->metrics_controller->distanceCalculation($value->origin_lat, $value->origin_lng, $value->destination_lat,  $value->destination_lng); // Calcular la distancia en kilómetros (por defecto)
                $sum_distance += $km;
                $sum_cost += $value->cost;
            }

            $sum_distance2= number_format((float)$sum_distance, 2, '.', '').' km';
            $sum_cost_total = number_format((float)$sum_cost, 2, '.', '');

            return response()->json([
                'data' => array("balance" => $sum_cost_total, "total_trips" => $sum_trips, "distance" => $sum_distance2)
            ], 200);
        }else{
            return response()->json([
                'data' => [],
            ], 200);
        }
    }
}
