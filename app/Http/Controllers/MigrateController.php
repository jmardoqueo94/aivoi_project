<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Driver;
use App\DriverCars;
use App\Customer;
use App\UserRoles;
use Hash;
use App\Providers\RouteServiceProvider;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class MigrateController extends Controller
{
    public function migrate(){
        $users = DB::connection('mysql2')->select('SELECT driver.id as driver_id, driver.nombre, driver.email, driver.telefono, driver.clave, driver.foto as photo_driver, driver.dui, driver.licencia, driver.solvencia, driver.antecedentes FROM driver INNER JOIN vechiculos ON vechiculos.user_id = driver.id WHERE clave IS NOT NULL GROUP BY driver.email ORDER BY driver.id');//GROUP BY driver.email ORDER BY id LIMIT 5
        $users = json_decode(json_encode($users), true);

        foreach ($users as $value) {
            
            $exist_email = User::where('email', $value['email'])->get()->count();

            if($exist_email == 0){
                $user = new User();
                $user->name = utf8_decode($value['nombre']);
                $user->email = $value['email'];
                $user->api_token = Str::random(40);
                $user->password = bcrypt($value['clave']);
                $user->status = 1;
                $user->save();

                if ($user) {
                    $user_role = new UserRoles();
                    $user_role->id_user = $user->id;
                    $user_role->id_role = 3;
                    $user_role->save();


                    $driver = new Driver();
                    $driver->phone = $value['telefono'];
                    $driver->photo = $value['photo_driver'];
                    $driver->photo_dui = $value['dui'];
                    $driver->photo_background = $value['antecedentes'];
                    $driver->photo_solvency_pnc = $value['solvencia'];
                    $driver->user_id = $user->id;
                    $driver->photo_licence = $value['licencia'];
                    $driver->save();

                    if($driver){

                        $cars = DB::connection('mysql2')->select('SELECT vechiculos.modelo, vechiculos.anio, vechiculos.color, vechiculos.placa, vechiculos.tarjeta, vechiculos.foto as foto_carro FROM vechiculos INNER JOIN driver ON driver.id = vechiculos.user_id where driver.id='.$value['driver_id']);
                        $cars = json_decode(json_encode($cars), true);
                        
                        if(count($cars) > 0){
                            $collection = collect($cars);
                            $unique = $collection->sortByDesc('id')->unique('placa')->toArray();
                            $res = array_values($unique);
                            
                            foreach ($res as $row) {
                                $car = new DriverCars();
                                $car->model_car = $row['modelo'];
                                $car->brand_car = 'predefinido';
                                $car->placa = $row['placa'];
                                $car->type_car_id = 1;
                                $car->year = $row['anio'];
                                $car->color = $row['color'];
                                $car->id_driver = $driver->id;
                                $car->status = 0;
                                $car->photo_card_circulation = isset($row['tarjeta']) ? $row['tarjeta'] : null;
                                $car->save();
                            }
                        }else{
                            break;
                        }

                    }else{
                        break;
                    }
                }else{
                    break;
                }
            }else{
                \Log::info("Ya existe el correo");
                break;
            }
        }

        return "HECHO";
    }

    public function migrateUsers(){

        $users = DB::connection('mysql2')->select('SELECT id, nombre, apellido, correo, foto, telefono, dui, clave FROM user WHERE nombre != "" AND apellido != "" AND correo != "" AND clave != "" AND telefono != "" ORDER BY id');//GROUP BY driver.email ORDER BY id LIMIT 5
        $users = json_decode(json_encode($users), true);
        \Log::info(count($users));

        foreach ($users as $value) {
            
            $exist_email = User::where('email', $value['correo'])->get()->count();

            if($exist_email == 0){
                $name = $value['nombre'].' '.$value['apellido'];

                $user = new User();
                $user->name = utf8_decode($name);
                $user->email = $value['correo'];
                $user->api_token = Str::random(40);
                $user->password = bcrypt($value['clave']);
                $user->status = 1;
                $user->save();

                if ($user) {
                    $user_role = new UserRoles();
                    $user_role->id_user = $user->id;
                    $user_role->id_role = 2;
                    $user_role->save();

                    $customer = new Customer();
                    $customer->phone = $value['telefono'];
                    $customer->photo = isset($value['foto']) ? $value['foto'] : null;
                    $customer->dui = isset($value['dui']) ? $value['dui'] : null;
                    $customer->user_id = $user->id;
                    $customer->save();

                }else{
                    continue;
                }
            }else{
                \Log::info("Ya existe el correo".$value['correo']);
                continue;
            }
        }
        
        return "HECHO";
    }

    public function activeCars(){
        
        $cars = DB::connection('mysql2')->select('SELECT vechiculos.placa, vechiculos.predefinido FROM vechiculos INNER JOIN driver ON driver.id = vechiculos.user_id');
        $cars = json_decode(json_encode($cars), true);

        foreach ($cars as $value) {
            DriverCars::where('placa', $value['placa'])
            ->update(['status' => $value['predefinido']]);
        }
        return "HECHO";
    }
}
