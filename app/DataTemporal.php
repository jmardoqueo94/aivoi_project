<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataTemporal extends Model
{
    protected $table = "user_data_temp";
}
