<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripsLogs extends Model
{
    protected $table = "trips_log";
}
