<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceVariants extends Model
{
    protected $table = "price_variants";
}
