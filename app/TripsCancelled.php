<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripsCancelled extends Model
{
    protected $table = "trips_cancelled";
}
