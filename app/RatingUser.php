<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RatingUser extends Model
{
    protected $table = "rating_users";
}
