<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TotalUsers extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = 'info@aivoiapp.com';
        $subject = 'Usuarios registrados HOY';
        $name = 'Usuarios AIVOI';

        return $this->view('emails.total_users_day')
            ->from($address, $name)
            ->subject($subject)
            ->with([ 'test_message' => 'test', 'drivers' => 3, 'customer' => 5 ]);
    }
}
