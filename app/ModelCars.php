<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelCars extends Model
{
    protected $table = "models_car";
}
