<nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile not-navigation-link">
      <div class="nav-link">
        <div class="user-wrapper" style="height: 115px;">
          <div class="profile-image" style="width: 100px !important; position: absolute;">
            @if(isset(Auth::user()->profile_url))
              <img src="data:image/png;base64, {{ Auth::user()->profile_url }}" alt="Image Preview" />
            @else
              <img src="{{ url('assets/images/faces/face8.jpg') }}" alt="profile image">
            @endif
          </div>
          <div class="text-wrapper" style="margin-top: 110px; margin-left: 0px !important">
            <p class="profile-name">{{ auth()->user()->name }}</p>
            <div class="dropdown" data-display="static">
              <a href="#" class="nav-link d-flex user-switch-dropdown-toggler" id="UsersettingsDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <small class="designation text-muted">Administrator</small>
                <span class="status-indicator online"></span>
              </a>
              <div class="dropdown-menu" aria-labelledby="UsersettingsDropdown">
                <a class="dropdown-item p-0">
                  <div class="d-flex border-bottom">
                    <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                      <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                    </div>
                    <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                      <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                    </div>
                    <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                      <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                    </div>
                  </div>
                </a>
                <!--<a class="dropdown-item mt-2"> Manage Accounts </a>
                <a class="dropdown-item"> Change Password </a>
                <a class="dropdown-item"> Check Inbox </a>-->
                <a class="dropdown-item"> Cerrar Sesión </a>
              </div>
            </div>
          </div>
        </div>
        
        </button>
      </div>
    </li>
    <li class="nav-item {{ active_class(['/']) }}">
      <a class="nav-link" href="{{ url('/') }}">
        <i class="menu-icon mdi mdi-television"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>

    <li class="nav-item {{ active_class(['users/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#users" aria-expanded="{{ is_active_route(['users/*']) }}" aria-controls="users">
        <i class="menu-icon mdi mdi-account-multiple"></i>
        <span class="menu-title">Usuarios</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['users/*']) }}" id="users">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['users/drivers']) }}">
            <a class="nav-link" href="{{ url('/users/drivers') }}"><i class="menu-icon mdi mdi-car"></i>Drivers</a>
          </li>
          <li class="nav-item {{ active_class(['users/customers']) }}">
            <a class="nav-link" href="{{ url('/users/customers') }}"><i class="menu-icon mdi mdi-human-male-female"></i>Clientes</a>
          </li>
          <li class="nav-item {{ active_class(['users/admin']) }}">
            <a class="nav-link" href="{{ url('/users/admin') }}"><i class="menu-icon mdi mdi-account-key"></i>Administradores</a>
          </li>
          <li class="nav-item {{ active_class(['users/suspended']) }}">
            <a class="nav-link" href="{{ url('/users/suspended') }}"><i class="menu-icon mdi mdi-account-remove"></i>Usuarios suspendidos</a>
          </li>
        </ul>
      </div>
    </li>

    <li class="nav-item {{ active_class(['reports/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="{{ is_active_route(['reports/*']) }}" aria-controls="reports">
        <i class="menu-icon mdi mdi-format-list-bulleted"></i>
        <span class="menu-title">Reportes</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['reports/*']) }}" id="reports">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['reports/report_trips']) }}">
            <a class="nav-link" href="{{ url('/reports/report_trips') }}"><i class="menu-icon mdi mdi-format-list-bulleted"></i>Reportes de clientes</a>
          </li>
          {{-- <li class="nav-item {{ active_class(['reports/customers']) }}">
            <a class="nav-link" href="#"><i class="menu-icon mdi mdi-format-list-bulleted"></i>Reportes de Clientes</a>
          </li> --}}
          <li class="nav-item {{ active_class(['reports/report_trips_driver']) }}">
            <a class="nav-link" href="{{ url('/reports/report_trips_driver') }}"><i class="menu-icon mdi mdi-format-list-bulleted"></i>Reportes de drivers</a>
          </li>
        </ul>
      </div>
    </li>
    
    <li class="nav-item {{ active_class(['track/track_driver']) }}">
      <a class="nav-link" href="{{ url('track/track_driver') }}">
        <i class="menu-icon mdi mdi-google-maps"></i>
        <span class="menu-title">Rastreo Driver</span>
      </a>
    </li>

    <li class="nav-item {{ active_class(['emprendedores/index']) }}">
      <a class="nav-link" href="{{ url('emprendedores/index') }}">
        <i class="menu-icon mdi mdi-format-list-bulleted"></i>
        <span class="menu-title">Emprendedores</span>
      </a>
    </li>


    <li class="nav-item {{ active_class(['settings/index']) }}">
      <a class="nav-link" href="{{ url('settings/index') }}">
        <i class="menu-icon mdi mdi-settings"></i>
        <span class="menu-title">Configuración</span>
      </a>
    </li>

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <li class="nav-item {{ active_class(['logout/logout']) }}">
      <a class="nav-link" href="{{ route('logout') }}">
        <i class="menu-icon mdi mdi-logout"></i>
        <span class="menu-title">Cerrar sesión</span>
      </a>
    </li>

    <!--<li class="nav-item {{ active_class(['basic-ui/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#basic-ui" aria-expanded="{{ is_active_route(['basic-ui/*']) }}" aria-controls="basic-ui">
        <i class="menu-icon mdi mdi-dna"></i>
        <span class="menu-title">Basic UI Elements</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['basic-ui/*']) }}" id="basic-ui">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['basic-ui/buttons']) }}">
            <a class="nav-link" href="{{ url('/basic-ui/buttons') }}">Buttons</a>
          </li>
          <li class="nav-item {{ active_class(['basic-ui/dropdowns']) }}">
            <a class="nav-link" href="{{ url('/basic-ui/dropdowns') }}">Dropdowns</a>
          </li>
          <li class="nav-item {{ active_class(['basic-ui/typography']) }}">
            <a class="nav-link" href="{{ url('/basic-ui/typography') }}">Typography</a>
          </li>
        </ul>
      </div>
    </li>

    <li class="nav-item {{ active_class(['charts/chartjs']) }}">
      <a class="nav-link" href="{{ url('/charts/chartjs') }}">
        <i class="menu-icon mdi mdi-chart-line"></i>
        <span class="menu-title">Charts</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['tables/basic-table']) }}">
      <a class="nav-link" href="{{ url('/tables/basic-table') }}">
        <i class="menu-icon mdi mdi-table-large"></i>
        <span class="menu-title">Tables</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['icons/material']) }}">
      <a class="nav-link" href="{{ url('/icons/material') }}">
        <i class="menu-icon mdi mdi-emoticon"></i>
        <span class="menu-title">Icons</span>
      </a>
    </li>-->
    {{-- <li class="nav-item {{ active_class(['user-pages/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#user-pages" aria-expanded="{{ is_active_route(['user-pages/*']) }}" aria-controls="user-pages">
        <i class="menu-icon mdi mdi-lock-outline"></i>
        <span class="menu-title">User Pages</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['user-pages/*']) }}" id="user-pages">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['user-pages/login']) }}">
            <a class="nav-link" href="{{ url('/user-pages/login') }}">Login</a>
          </li>
          <li class="nav-item {{ active_class(['user-pages/register']) }}">
            <a class="nav-link" href="{{ url('/user-pages/register') }}">Register</a>
          </li>
          <li class="nav-item {{ active_class(['user-pages/lock-screen']) }}">
            <a class="nav-link" href="{{ url('/user-pages/lock-screen') }}">Lock Screen</a>
          </li>
        </ul>
      </div>
    </li> --}}
    {{-- <li class="nav-item">
      <a class="nav-link" href="https://www.bootstrapdash.com/demo/star-laravel-free/documentation/documentation.html" target="_blank">
        <i class="menu-icon mdi mdi-file-outline"></i>
        <span class="menu-title">Documentation</span>
      </a>
    </li> --}}
  </ul>
</nav>