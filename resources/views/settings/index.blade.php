@extends('layout.master')
<style>
    .form-control {
        display: block;
        width: 100%;
        height: 2.5rem !important;
        padding: 0.875rem 1.375rem;
        font-size: 0.75rem;
        font-weight: 400;
        line-height: 1;
        color: #495057;
        background-color: #ffffff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 8px !important;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    

</style>

@push('plugin-styles')
@endpush

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Configuración General</h4>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Driver %</th>
                            <th>Trafico %</th>
                            <th>Distancia Corta ($ x km)</th>
                            <th>Distancia Larga ($ x km)</th>
                            <th>Minuto ($)</th>
                            <th>Base ($)</th>
                            <th>IVA ($)</th>
                            <th>Cobro Tarjeta ($)</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($settings) && $settings->count())
                                @foreach ($settings as $sett)
                                    <tr>
                                        <td>{{$sett->driver}}%</td>
                                        <td>{{$sett->traffic}}%</td>
                                        <td>${{$sett->short_distance}}</td>
                                        <td>${{$sett->long_distance}}</td>
                                        <td>${{$sett->min}}</td>
                                        <td>${{$sett->base}}</td>
                                        <td>${{$sett->iva}}</td>
                                        <td>${{$sett->credit_card}}</td>
                                        <td>
                                            <i class="menu-icon mdi mdi-lead-pencil" onclick="editModal({{$sett->id}})" style="font-size: 14pt"></i>
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="editComision{{$sett->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Editar comisión</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="hidden" name="setting_id" id="setting_id_{{$sett->id}}" value="{{$sett->id}}">
                                                    <div class="form-group">
                                                        <label for="email">Driver %</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="driver_{{$sett->id}}" name="driver" value="{{$sett->driver}}" placeholder="Valor 0.00">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Tráfico %</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="traffic_{{$sett->id}}" name="traffic" value="{{$sett->traffic}}" placeholder="Valor 0.00">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Distancia Corta ($ x km)</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="short_distance_{{$sett->id}}" name="short_distance" value="{{$sett->short_distance}}" placeholder="Valor 0.00">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Distancia Larga ($ x km)</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="long_distance_{{$sett->id}}" name="long_distance" value="{{$sett->long_distance}}" placeholder="Valor 0.00">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Minuto ($)</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="min_{{$sett->id}}" name="min" value="{{$sett->min}}" placeholder="Valor 0.00">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Base ($)</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="base_{{$sett->id}}" name="base" value="{{$sett->base}}" placeholder="Valor 0.00">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">IVA ($)</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="iva_{{$sett->id}}" name="iva" value="{{$sett->iva}}" placeholder="Valor 0.00">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Tarjeta de crédito ($)</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="credit_card_{{$sett->id}}" name="credit_card" value="{{$sett->credit_card}}" placeholder="Valor 0.00">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" onclick="editComission({{$sett->id}})" class="btn btn-primary">Editar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">No hay datos que mostrar.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush


<script>
    function editModal(id){
        $('#editComision'+id).appendTo("body").modal('show');
    }

    function editComission(id) {
        Swal.fire({
            title: 'Quieres cambiar el valor de la comisión?',
            text: "No se podrá deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero cambiarla! '
        }).then((result) => {
            if (result.isConfirmed) {

                let _token   = $('meta[name="csrf-token"]').attr('content');

                /*Codigo ajax*/
                $.ajax({
                    url:"{{ route('edit_settings') }}",
                    type:"POST",
                    data:{
                        id: id,
                        driver: $("#driver_"+id).val(),
                        traffic: $("#traffic_"+id).val(),
                        short_distance: $("#short_distance_"+id).val(),
                        long_distance: $("#long_distance_"+id).val(),
                        min: $("#min_"+id).val(),
                        base: $("#base_"+id).val(),
                        iva: $("#iva_"+id).val(),
                        credit_card: $("#credit_card_"+id).val(),
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success == true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Los valores de comisión fueron editados',
                                showConfirmButton: true,
                                timer: 5000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            })
                        }else{
                            Swal.fire(
                                'Error!',
                                'No se pudo cambiar, contacte a soporte.',
                                'Cancelled'
                            )
                        }
                    },
                });
            }
        })
    }
</script>