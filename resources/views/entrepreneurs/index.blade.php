@extends('layout.master')
<style>
    .form-control {
        display: block;
        width: 100%;
        height: 2.5rem !important;
        padding: 0.875rem 1.375rem;
        font-size: 0.75rem;
        font-weight: 400;
        line-height: 1;
        color: #495057;
        background-color: #ffffff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 8px !important;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    

</style>

@push('plugin-styles')
@endpush

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">LISTA DE EMPRENDEDORES</h4>
                <button class="btn btn-primary float-right" id="openModal" >
                    <strong>Nuevo emprendedor</strong> 
                </button>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Dirección</th>
                            <th>Nombre contacto</th>
                            <th>Número contacto</th>
                            <th>Fecha creación</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($emprendedor) && $emprendedor->count())
                                @foreach ($emprendedor as $empr)
                                    <tr>
                                        <td>{{$empr->id}}</td>
                                        <td>{{$empr->name}}</td>
                                        <td>{{$empr->address}}</td>
                                        <td>{{$empr->contact_name}}</td>
                                        <td>{{$empr->phone}}</td>
                                        <td>{{$empr->created_at}}</td>
                                        <td>
                                            <i class="menu-icon mdi mdi-lead-pencil" onclick="editModal({{$empr->id}})" style="font-size: 14pt"></i>
                                            &nbsp;&nbsp;
                                            <i class="menu-icon mdi mdi-delete-forever" onclick="eliminarEmprendedor({{$empr->id}})" style="font-size: 14.5pt"></i>
                                            
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="editEmprendedor{{$empr->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content" style="background-color: #fff !important;">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Editar emprendedor</h4>
                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                </div>
                                                
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <div id="pb-modalreglog-progressbar"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Nombre</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="hidden" name="edit_emprendedor_id" id="emprendedor_id_{{$empr->id}}" value="{{$empr->id}}">
                                                            <input type="text" class="form-control" id="edit_name_{{$empr->id}}" name="edit_name" value="{{$empr->name}}" placeholder="Nombre">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Correo</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="email" class="form-control" id="edit_email_{{$empr->id}}" name="edit_email" value="{{$empr->email}}" placeholder="Correo">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password">Dirección</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="edit_address_{{$empr->id}}" name="edit_address" value="{{$empr->address}}" placeholder="Dirección">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="text">Teléfono</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="edit_phone_{{$empr->id}}" name="edit_phone" value="{{$empr->phone}}" placeholder="Teléfono">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="text">Nombre contacto</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <input type="text" class="form-control" id="edit_contact_name_{{$empr->id}}" name="edit_contact_name" value="{{$empr->contact_name}}" placeholder="Contacto">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" onclick="editEmprendedor({{$empr->id}})" class="btn btn-primary">Editar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                
                                    <div class="modal fade" id="newEmpr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content" style="background-color: #fff !important;">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Nuevo emprendedor</h4>
                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                </div>
                                                <form method="POST" action="{{ route('register_entrepreneurs') }}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <div id="pb-modalreglog-progressbar"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">Nombre</label>
                                                            <div class="input-group pb-modalreglog-input-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">Correo</label>
                                                            <div class="input-group pb-modalreglog-input-group">
                                                                <input type="email" class="form-control" id="email" name="email" placeholder="Correo">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="password">Dirección</label>
                                                            <div class="input-group pb-modalreglog-input-group">
                                                                <input type="text" class="form-control" id="address" name="address" placeholder="Dirección">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="text">Teléfono</label>
                                                            <div class="input-group pb-modalreglog-input-group">
                                                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Teléfono">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="text">Nombre contacto</label>
                                                            <div class="input-group pb-modalreglog-input-group">
                                                                <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Contacto">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Guardar</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">No hay datos que mostrar.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
    <audio id="audio" controls style="display: none;">
        <source type="audio/mp3" src="{{ asset('assets/audio/alarma.mp3') }}">
    </audio>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

<script>
    $(document).ready(function(){
        $( "#openModal" ).click(function() {
            $('#newEmpr').appendTo("body").modal('show');
        });
    });

    function editModal(id){
        $('#editEmprendedor'+id).appendTo("body").modal('show');
    }

    function eliminarEmprendedor(id) {
        Swal.fire({
            title: 'Quieres eliminar este emprendedor?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero eliminarlo! '
        }).then((result) => {
            if (result.isConfirmed) {

                let _token   = $('meta[name="csrf-token"]').attr('content');

                /*Codigo ajax*/
                $.ajax({
                    url:"{{ route('delete_emprendedor') }}",
                    type:"POST",
                    data:{
                        id: id,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success == true) {
                            Swal.fire({
                                icon: 'success',
                                title: response.data,
                                showConfirmButton: true,
                                timer: 5000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            })
                        }else{
                            Swal.fire(
                                'Activo!',
                                response.data,
                                'Cancelled'
                            )
                        }
                    },
                });
            }
        })
    }

    function editEmprendedor(id) {
        Swal.fire({
            title: 'Quieres editar este emprendedor?',
            text: "No se podrá deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero editarlo! '
        }).then((result) => {
            if (result.isConfirmed) {

                let _token   = $('meta[name="csrf-token"]').attr('content');

                /*Codigo ajax*/
                $.ajax({
                    url:"{{ route('edit_entrepreneurs') }}",
                    type:"POST",
                    data:{
                        id: id,
                        name: $("#edit_name_"+id).val(),
                        email: $("edit_#email_"+id).val(),
                        address: $("#edit_address_"+id).val(),
                        phone: $("#edit_phone_"+id).val(),
                        contact_name: $("#edit_contact_name_"+id).val(),
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success == true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'El emprendedor fue editado',
                                showConfirmButton: true,
                                timer: 5000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            })
                        }else{
                            Swal.fire(
                                'Error!',
                                'No se pudo editar, contacte a soporte.',
                                'Cancelled'
                            )
                        }
                    },
                });
            }
        })
    }

</script>