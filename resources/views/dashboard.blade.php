@extends('layout.master')

@push('plugin-styles')
  <!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
  <style>
    .card-revenue {
      background: linear-gradient(120deg, #183a56, #08658c, #18bef1) !important;
    }
  </style>
@endpush

@section('content')
<div class="row">
  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
          <div class="float-left">
            <i class="mdi mdi-cube text-danger icon-lg"></i>
          </div>
          <div class="float-right">
            <p class="mb-0 text-right">Viajes del día</p>
            <div class="fluid-container">
              <h3 class="font-weight-medium text-right mb-0">{{$trips_day}}</h3>
            </div>
          </div>
        </div>
        {{-- <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">
          <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 65% lower growth </p> --}}
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
          <div class="float-left">
            <i class="mdi mdi-receipt text-warning icon-lg"></i>
          </div>
          <div class="float-right">
            <p class="mb-0 text-right">Cobros Realizados en el día</p>
            <div class="fluid-container">
              <h3 class="font-weight-medium text-right mb-0">${{number_format((float)$payment_day, 2, '.', '')}}</h3>
            </div>
          </div>
        </div>
        {{-- <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">
          <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Product-wise sales </p> --}}
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
          <div class="float-left">
            <i class="mdi mdi-poll-box text-success icon-lg"></i>
          </div>
          <div class="float-right">
            <p class="mb-0 text-right">Clientes Activos</p>
            <div class="fluid-container">
              <h3 class="font-weight-medium text-right mb-0">{{$customer_active}}</h3>
            </div>
          </div>
        </div>
        {{-- <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">
          <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Weekly Sales </p> --}}
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
          <div class="float-left">
            <i class="mdi mdi-account-box-multiple text-info icon-lg"></i>
          </div>
          <div class="float-right">
            <p class="mb-0 text-right">Drivers Activos</p>
            <div class="fluid-container">
              <h3 class="font-weight-medium text-right mb-0">{{$driver_active}}</h3>
            </div>
          </div>
        </div>
        {{-- <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">
          <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Product-wise sales </p> --}}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Viajes Activos</h4>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> # </th>
                <th> Nombre Cliente </th>
                <th> Nº Factura </th>
                <th> Driver </th>
                <th> Total a pagar </th>
                <th> Estado </th>
                <th> Fecha y Hora </th>
              </tr>
            </thead>
            <tbody>
              @if(!empty($arr_active))
                @foreach ($arr_active as $active)
                <tr>
                  <td>{{$active->id}}</td>
                  <td>{{$active->customer_name}}</td>
                  <td>{{$active->bill_trip}}</td>
                  <td>{{$active->driver_name}}</td>
                  <td>${{number_format((float)$active->cost, 2, '.', '')}}</td>
                  <td>@if($active->last_status < 4)En Proceso @endif</td>
                  <td>{{$active->created_at}}</td>
                </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="7">No hay datos que mostrar</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-6 col-xl-8 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Viajes terminados en el día</h4>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> # </th>
                <th> Nombre Cliente </th>
                <th> Nº Factura </th>
                <th> Driver </th>
                <th> Total a pagar </th>
                <th> Estado </th>
                <th> Fecha y Hora </th>
              </tr>
            </thead>
            <tbody>
              @if(!empty($arr_finish))
                @foreach ($arr_finish as $finish)
                <tr>
                  <td>{{$finish->id}}</td>
                  <td>{{$finish->customer_name}}</td>
                  <td>{{$finish->bill_trip}}</td>
                  <td>{{$finish->driver_name}}</td>
                  <td>${{number_format((float)$finish->cost, 2, '.', '')}}</td>
                  <td>@if($finish->last_status == 4)Completado @endif</td>
                  <td>{{$finish->created_at}}</td>
                </tr>
                @endforeach
              @else
                <tr>
                    <td colspan="7">No hay datos que mostrar</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-xl-4 grid-margin stretch-card">
    <div class="row flex-grow">
      <div class="col-md-6 col-xl-12 grid-margin grid-margin-md-0 grid-margin-xl stretch-card">
        <div class="card card-revenue">
          <div class="card-body d-flex align-items-center">
            <div class="d-flex flex-grow">
              <div class="mr-auto">
                <p class="highlight-text mb-0 text-white"> ${{number_format((float)$payment_month, 2, '.', '')}}</p>
                <p class="text-white"> Cobro total en el mes </p>
                {{-- <div class="badge badge-pill"> 18% </div> --}}
              </div>
              <div class="ml-auto align-self-end">
                <div id="revenue-chart" sparkType="bar" sparkBarColor="#e6ecf5" barWidth="2"> 4,3,10,9,4,3,8,6,7,8 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xl-12 stretch-card">
        <div class="card card-revenue-table">
          <div class="card-body">
            <div class="revenue-item d-flex">
              <div class="revenue-desc">
                <h6>Total cobros en efectivo</h6>
                <p class="font-weight-light"> En el mes </p>
              </div>
              <div class="revenue-amount">
                <p class="text-primary"> ${{number_format((float)$payment_cash_month, 2, '.', '')}}</p>
              </div>
            </div>
            <div class="revenue-item d-flex">
              <div class="revenue-desc">
                <h6>Total cobros con tarjeta</h6>
                <p class="font-weight-light"> En el mes </p>
              </div>
              <div class="revenue-amount">
                <p class="text-primary"> ${{number_format((float)$payment_card_month, 2, '.', '')}} </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Drivers con más viajes en el día</h4>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> # </th>
                <th> Nombre Driver </th>
                <th> # de viajes </th>
                <th> Total ganado </th>
                <th> Distancia recorrida </th>
              </tr>
            </thead>
            <tbody>
              @if(!empty($driver_trips))
                @foreach ($driver_trips as $trip)
                <tr>
                  <td>{{$trip->driver_id}}</td>
                  <td>{{$trip->driver_name}}</td>
                  <td>{{$trip->total_trips}}</td>
                  <td>${{number_format((float)$trip->total_costo, 2, '.', '')}}</td>
                  <td>175km</td>
                </tr>
                @endforeach
              @else
                <tr>
                    <td colspan="7">No hay datos que mostrar</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('plugin-scripts')
  {!! Html::script('/assets/plugins/chartjs/chart.min.js') !!}
  {!! Html::script('/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
  {!! Html::script('/assets/js/dashboard.js') !!}
@endpush