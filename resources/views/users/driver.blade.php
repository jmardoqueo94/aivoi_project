@extends('layout.master')

@push('plugin-styles')
@endpush

<style>
    .img-size{
    /* 	padding: 0;
        margin: 0; */
        /height: 100%;
        width: 100%;
        background-size: cover;
        overflow: hidden;
    }

    img.zoom {
        width: 350px;
        height: 200px;
        -webkit-transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        -ms-transition: all .2s ease-in-out;
    }
    
    .transition {
        -webkit-transform: scale(1.8); 
        -moz-transform: scale(1.8);
        -o-transform: scale(1.8);
        transform: scale(1.8);
    }

    .modal-body {
        padding: 0;
    }

    .carousel-control-prev-icon {
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23009be1' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
        width: 30px;
        height: 48px;
    }
    .carousel-control-next-icon {
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23009be1' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
        width: 30px;
        height: 48px;
    }

    .form-control {
        display: block;
        width: 100%;
        height: 2.5rem !important;
        padding: 0.875rem 1.375rem;
        font-size: 0.75rem;
        font-weight: 400;
        line-height: 1;
        color: #495057;
        background-color: #ffffff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 8px !important;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif

<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">LISTA DE DRIVERS</h4>
                <form action="{{ route('search_driver') }}" method="POST" role="search">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="driver_search" placeholder="Search driver">
                        </div>
                        <div class="col-md-1" style="padding: 5px;">
                            <input type="submit" class="btn btn-success" value="Buscar">
                        </div>
                        
                        <div class="col-md-1" style="padding: 5px;">
                            <a href="{{ route('drivers') }}" class="btn btn-primary pull-right">Reset</a>
                        </div>
                    </div>
                </form>
                <br>
                <div class="table-responsive">
                    <table id="driver_list" class="table table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                            <th>Foto</th>
                            <th>Documentos</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($drivers) && $drivers->count())
                                @foreach ($drivers as $driver)
                                    <tr>
                                        <td>{{$driver->id}}</td>
                                        <td>{{$driver->name}}</td>
                                        <td>{{$driver->email}}</td>
                                        <td>{{$driver->phone}}</td>
                                        @if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $driver->photo))
                                            <td><img src="data:image/png;base64, {{$driver->photo}}" alt="Driver perfil" width="100" class="zoom"/></td>
                                        @else
                                            @if(empty($driver->photo) || $driver->photo == "url_de_la_foto")
                                                <td><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgBhcplevwUKGRs1P-Ps8Mwf2wOwnW_R_JIA&usqp=CAU" width="100" class="zoom"></td>
                                            @else
                                                <td><img src="{{$driver->photo}}" alt='Driver perfil' width="100" class="zoom"></td>
                                            @endif
                                        @endif

                                        <td>
                                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-id="{{ $driver->id }}" data-target="#docsModal{{$driver->id}}">
                                                <strong>Ver docs</strong> 
                                            </button>
                                        </td>
                                        <td>
                                            @if($driver->suspended == 1)
                                                <label class="badge badge-danger">Suspendido</label>
                                            @elseif($driver->status == 0)
                                                <label class="badge badge-danger">Pendiente</label>
                                            @else
                                                <label class="badge badge-success">Activo</label>
                                            @endif
                                        </td>
                                        <td>
                                            <i class="menu-icon mdi mdi-key" data-toggle="modal" data-target="#changePassword{{$driver->id}}"  style="font-size: 14pt" title="Cambiar Contraseña"></i>
                                            &nbsp;&nbsp;&nbsp;
                                            @if($driver->status == 0)
                                                <i class="menu-icon mdi mdi-account-check" onclick="activarDriver({{$driver->id}})" style="font-size: 15.5pt" title="Activar Driver"></i>
                                            @else
                                                <i class="menu-icon mdi mdi-block-helper" onclick="desactivarDriver({{$driver->id}})" style="font-size: 14.5pt;" title="Desactivar Driver"></i>
                                            @endif
                                            &nbsp;&nbsp;&nbsp;
                                            @if($driver->suspended == 0)
                                                <i class="menu-icon mdi mdi mdi-account-off" data-toggle="modal" data-target="#suspendedDriver{{$driver->id}}" style="font-size: 15.5pt; color:red;" title="Suspender Driver"></i>
                                            @else
                                                <i class="menu-icon mdi mdi mdi-account-convert" onclick="revalidadDriver({{$driver->id}})" style="font-size: 14.5pt; color:green;" title="Activar Driver"></i>
                                            @endif
                                            &nbsp;&nbsp;&nbsp;
                                            <i class="menu-icon mdi mdi-message-text" data-toggle="modal" data-target="#sendNotification{{$driver->id}}"  style="font-size: 14pt" title="Enviar notificación"></i>
                                        </td>
                                    </tr>
                                    <input type="hidden" name="name_driver{{$driver->id}}" id="name_driver{{$driver->id}}" value="{{$driver->name}}">
                                    <input type="hidden" name="email_driver{{$driver->id}}" id="email_driver{{$driver->id}}" value="{{$driver->email}}">

                                    <!--AQUI VA EL MODAL DE LOS DOCUMENTOS-->
                                    <div id="docsModal{{$driver->id}}" class="modal fade" role="dialog">
                                        <div class="modal-dialog" >
                                            <!-- Modal content-->
                                            <div class="modal-content" style="border:none;">
                                                <div class="modal-header">
                                                    <h5>Documentos de {{$driver->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <div id='carouselExampleIndicators_{{$driver->id}}' class='carousel slide' data-ride='carousel'>
                                                        <ol class='carousel-indicators'>
                                                            <li data-target='#carouselExampleIndicators_{{$driver->id}}' data-slide-to='0' class='active'></li>
                                                            <li data-target='#carouselExampleIndicators_{{$driver->id}}' data-slide-to='1' ></li>
                                                            <li data-target='#carouselExampleIndicators_{{$driver->id}}' data-slide-to='2' ></li>
                                                            <li data-target='#carouselExampleIndicators_{{$driver->id}}' data-slide-to='3' ></li>
                                                        </ol>
                                                        <div class='carousel-inner'>
                                                            <div class='carousel-item active'>
                                                                @if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $driver->photo_dui))
                                                                    <img class='img-size' src="data:image/png;base64, {{$driver->photo_dui}}" alt="DUI"/>
                                                                @else
                                                                    @if(empty($driver->photo_dui) || $driver->photo_dui == "url_de_la_foto")
                                                                        <img class='img-size' src="https://cdn0.iconfinder.com/data/icons/file-and-document-41/100/file_document_doc-23-512.png">
                                                                    @else
                                                                        <img class='img-size' src="{{$driver->photo_dui}}" alt='DUI'>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                            <div class='carousel-item'>
                                                                @if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $driver->photo_licence))
                                                                    <img class='img-size' src="data:image/png;base64, {{$driver->photo_licence}}" alt="Licencia de conducir"/>
                                                                @else
                                                                    @if(empty($driver->photo_licence) || $driver->photo_licence == "url_de_la_foto")
                                                                        <img class='img-size' src="https://cdn0.iconfinder.com/data/icons/file-and-document-41/100/file_document_doc-23-512.png">
                                                                    @else
                                                                        <img class='img-size' src="{{$driver->photo_licence}}" alt='Licencia de conducir'>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                            <div class='carousel-item'>
                                                                @if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $driver->photo_background))
                                                                    <img class='img-size' src="data:image/png;base64, {{$driver->photo_background}}" alt="Antecedentes Penales"/>
                                                                @else
                                                                    @if(empty($driver->photo_background) || $driver->photo_background == "url_de_la_foto")
                                                                        <img class='img-size' src="https://cdn0.iconfinder.com/data/icons/file-and-document-41/100/file_document_doc-23-512.png">
                                                                    @else
                                                                        <img class='img-size' src="{{$driver->photo_background}}" alt='Antecedentes Penales'>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                            <div class='carousel-item'>
                                                                @if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $driver->photo_solvency_pnc))
                                                                    <img class='img-size' src="data:image/png;base64, {{$driver->photo_solvency_pnc}}" alt="Solvencia PNC"/>
                                                                @else
                                                                    @if(empty($driver->photo_solvency_pnc) || $driver->photo_solvency_pnc == "url_de_la_foto")
                                                                        <img class='img-size' src="https://cdn0.iconfinder.com/data/icons/file-and-document-41/100/file_document_doc-23-512.png">
                                                                    @else
                                                                        <img class='img-size' src="{{$driver->photo_solvency_pnc}}" alt='Solvencia PNC'>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <a class='carousel-control-prev' href='#carouselExampleIndicators_{{$driver->id}}' role='button' data-slide='prev' >
                                                            <span class='carousel-control-prev-icon' aria-hidden='true' ></span>
                                                            <span class='sr-only'>Previous</span>
                                                        </a>
                                                        <a class='carousel-control-next' href='#carouselExampleIndicators_{{$driver->id}}' role='button' data-slide='next'>
                                                            <span class='carousel-control-next-icon' aria-hidden='true' ></span>
                                                            <span class='sr-only'>Next</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="suspendedDriver{{$driver->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content" style="background-color: #fff !important;">
                                                <div class="modal-header">
                                                    <h5>Suspender a driver {{$driver->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                </div>

                                                <div class="modal-body">
                                                    <input type="hidden" name="user_id" id="user_id_{{$driver->id}}" value="{{$driver->id}}">
                                                    <div class="form-group">
                                                        <label for="email">Razón de suspensión</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <select name="reason" id="reason_{{$driver->id}}" class="form-control" style="height: 2.8rem !important;">
                                                                <option value="Mal trato / Insulto">Mal trato / Insulto</option>
                                                                <option value="Involucrado en robo">Involucrado en robo</option>
                                                                <option value="Irrespetó normas">Irrespetó normas</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="comment">Explique lo sucedido</label>
                                                        <div class="input-group">
                                                            <input type="text" name="comentario_{{$driver->id}}" id="comentario_{{$driver->id}}" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" onclick="suspenderDriver({{$driver->id}})">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal" id="changePassword{{$driver->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title">Cambiar contraseña</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="comment">Ingrese la nueva contraseña</label>
                                                    <div class="input-group">
                                                        <input type="hidden" name="user_id_pwd" id="user_id_pwd{{$driver->id}}" value="{{$driver->id}}">
                                                        <input type="text" name="password_{{$driver->id}}" id="password_{{$driver->id}}" class="form-control" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-primary" onclick="changePwd({{$driver->id}})">Cambiar</button>
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            </div>
                                          </div>
                                        </div>
                                    </div>


                                    <div class="modal" id="sendNotification{{$driver->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title">Mensaje para driver</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="comment">Ingrese el mensaje</label>
                                                    <div class="input-group">
                                                        <input type="hidden" name="name_driver_{{$driver->id}}" id="name_driver_{{$driver->id}}" value="{{$driver->name}}">
                                                        <input type="text" name="message_{{$driver->id}}" id="message_{{$driver->id}}" class="form-control" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-primary" onclick="sendMessage({{$driver->id}})">Enviar</button>
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">No hay datos que mostrar.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $drivers->links() !!}
                </div>
                
            </div>
        </div>
    </div>
    <audio id="audio" controls style="display: none;">
        <source type="audio/mp3" src="{{ asset('assets/audio/alarma.mp3') }}">
    </audio>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

<script>

    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#driver_list tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $('.zoom').hover(function() {
            $(this).addClass('transition');
        }, function() {
            $(this).removeClass('transition');
        });

        setInterval(alarmaDriver, 50000);
    });

    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }

    function activarDriver(id) {
        Swal.fire({
            title: 'Quieres activar este driver?',
            text: "Podrá recibir viajes después de esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero activarlo! '
        }).then((result) => {
            if (result.isConfirmed) {

                let _token   = $('meta[name="csrf-token"]').attr('content');

                /*Codigo ajax*/
                $.ajax({
                    url:"{{ route('activate_driver') }}",
                    type:"POST",
                    data:{
                        id: id,
                        name_driver: $("#name_driver"+id).val(),
                        email_driver: $("#email_driver"+id).val(),
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success == true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'El driver seleccionado fue activado',
                                showConfirmButton: true,
                                timer: 5000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            })
                        }else{
                            Swal.fire(
                                'Activo!',
                                'El driver seleccionado no fue activado.',
                                'Cancelled'
                            )
                        }
                    },
                });
            }
        })
    }

    function desactivarDriver(id) {
        Swal.fire({
            title: 'Quieres desactivar este driver?',
            text: "No podrá recibir viajes después de esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero desactivarlo!'
        }).then((result) => {
            if (result.isConfirmed) {
                let _token   = $('meta[name="csrf-token"]').attr('content');

                /*Codigo ajax*/
                $.ajax({
                    url:"{{ route('desactivate_driver') }}",
                    type:"POST",
                    data:{
                    id: id,
                    _token: "{{ csrf_token() }}",
                    },
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'El driver seleccionado fue desactivado',
                                showConfirmButton: true,
                                timer: 5000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Activo!',
                                'El driver seleccionado no fue desactivado.',
                                'Cancelled'
                            )
                        }
                    },
                });
            }
        })
    }

    function suspenderDriver(id){
        
        Swal.fire({
            title: 'Quieres suspender este driver?',
            text: "No podrá recibir viajes después de esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero suspenderlo!'
        }).then((result) => {
            if (result.isConfirmed) {
                let _token   = $('meta[name="csrf-token"]').attr('content');
                //var id = $("#user_id_"+id).val();
                $.ajax({
                    url:"{{ route('suspend_user') }}",
                    type:"POST",
                    data:{
                        user_id: id,
                        reason: $("#reason_"+id).val(),
                        comment: $("#comentario_"+id).val(),
                        _token: "{{ csrf_token() }}",
                    },
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'El driver seleccionado fue suspendido',
                                showConfirmButton: true,
                                timer: 8000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Error!',
                                'El driver seleccionado no pudo fue suspendido.',
                                'Cancelled'
                            )
                        }
                    }
                });
            }
        })
    }

    function changePwd(id){
        
        Swal.fire({
            title: 'Quieres cambiar la contraseña del driver?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero cambiarla!'
        }).then((result) => {
            if (result.isConfirmed) {
                let _token   = $('meta[name="csrf-token"]').attr('content');
                //var id = $("#user_id_"+id).val();
                $.ajax({
                    url:"{{ route('change_pwd') }}",
                    type:"POST",
                    data:{
                    user_id: id,
                    password: $("#password_"+id).val(),
                    _token: "{{ csrf_token() }}",
                    },
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'La contraseña fue cambiada',
                                showConfirmButton: true,
                                timer: 8000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Error!',
                                'No se pudo cambiar la contraseña',
                                'Cancelled'
                            )
                        }
                    }
                });
            }
        })
    }

    function sendMessage(id){
        
        Swal.fire({
            title: 'Quieres enviar notificación al driver?',
            //text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero enviarla!'
        }).then((result) => {
            if (result.isConfirmed) {
                let _token   = $('meta[name="csrf-token"]').attr('content');
                //var id = $("#user_id_"+id).val();
                $.ajax({
                    url:"{{ route('send_notification') }}",
                    type:"POST",
                    data:{
                        user_id: id,
                        name_driver: $("#name_driver_"+id).val(),
                        message: $("#message_"+id).val(),
                        _token: "{{ csrf_token() }}",
                    },
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'La notificación fue enviada',
                                showConfirmButton: true,
                                timer: 8000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Error!',
                                response.success,
                                'Cancelled'
                            )
                        }
                    }
                });
            }
        })
    }

    function alarmaDriver(){
        let _token   = $('meta[name="csrf-token"]').attr('content');
        var audio = document.getElementById("audio");

        /*Codigo ajax*/
        $.ajax({
            url:"{{ route('verify_new_drivers') }}",
            type:"POST",
            data:{
                _token: "{{ csrf_token() }}",
            },
            success: function(response){
                console.log(response);
                if(response.data > 0){
                    
                    audio.play();
                    audio.loop =true;
                    Swal.fire({
                        icon: 'info',
                        title: 'Hay drivers recien registrados',
                        showConfirmButton: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            audio.pause();
                        }
                    })
                }
            },
        });
    }
    
</script>