@extends('layout.master')
<style>
    .form-control {
        display: block;
        width: 100%;
        height: 2.5rem !important;
        padding: 0.875rem 1.375rem;
        font-size: 0.75rem;
        font-weight: 400;
        line-height: 1;
        color: #495057;
        background-color: #ffffff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 8px !important;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }
</style>

@push('plugin-styles')
@endpush

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">USUARIOS SUSPENDIDOS</h4>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Rol</th>
                            <th>Motivo Suspensión</th>
                            <th>Comentario</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($users) && $users->count())
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->user_id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            @if($user->id_role == 2)
                                                <b>Cliente</b>
                                            @elseif($user->id_role == 3)
                                                <b>Driver</b>
                                            @else
                                                <b>Admin</b>
                                            @endif
                                        </td>
                                        <td>{{$user->reason}}</td>
                                        <td>{{$user->comment}}</td>
                                        <td>
                                            @if($user->suspended == 1)
                                                <label class="badge badge-danger">Suspendido</label>
                                            @else
                                                <label class="badge badge-success">Activo</label>
                                            @endif
                                        </td>
                                        <td>   
                                            &nbsp;&nbsp;
                                            <i class="menu-icon mdi mdi-reload" onclick="revalidarUser({{$user->user_id}})" style="font-size: 14.5pt; color:green;"></i>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">No hay datos que mostrar.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

<script>

function revalidarUser(id){
        
        Swal.fire({
            title: 'Quieres validar este usuario de nuevo?',
            text: "Se le quitará la suspensión",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero activarlo de nuevo!'
        }).then((result) => {
            if (result.isConfirmed) {
                let _token   = $('meta[name="csrf-token"]').attr('content');
                //var id = $("#user_id_"+id).val();
                $.ajax({
                    url:"{{ route('revalidate_user') }}",
                    type:"POST",
                    data:{
                        user_id: id,
                        _token: "{{ csrf_token() }}",
                    },
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'El usuario fue revalidado',
                                showConfirmButton: true,
                                timer: 8000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Error!',
                                'No se pudo revalidar el usuario.',
                                'Cancelled'
                            )
                        }
                    }
                });
            }
        })
    }
    
</script>