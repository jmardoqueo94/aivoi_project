@extends('layout.master')
<style>
    .form-control {
        display: block;
        width: 100%;
        height: 2.5rem !important;
        padding: 0.875rem 1.375rem;
        font-size: 0.75rem;
        font-weight: 400;
        line-height: 1;
        color: #495057;
        background-color: #ffffff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 8px !important;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }
</style>

@push('plugin-styles')
@endpush

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">LISTA DE ADMINISTRADORES</h4>
                <button class="btn btn-primary float-right" id="openModal">
                    <strong>Nuevo Administrador</strong> 
                </button>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($admins) && $admins->count())
                                @foreach ($admins as $admin)
                                    <tr>
                                        <td>{{$admin->id}}</td>
                                        <td>{{$admin->name}}</td>
                                        <td>{{$admin->email}}</td>
                                        <td>
                                            @if($admin->status == 0)
                                                <label class="badge badge-danger">Inactivo</label>
                                            @else
                                                <label class="badge badge-success">Activo</label>
                                            @endif
                                        </td>
                                        <td>
                                            @if(Auth::user()->id == $admin->id)
                                                <i class="menu-icon mdi mdi-lead-pencil" onclick="editModal({{$admin->id}})" style="font-size: 14pt"></i>
                                            @else
                                                <i class="menu-icon mdi mdi-lead-pencil"  style="font-size: 14pt; color:#ccc;"></i>
                                            @endif
                                            &nbsp;&nbsp;&nbsp;
                                            @if(Auth::user()->id == $admin->id)
                                                <i class="menu-icon mdi mdi-delete-forever" style="font-size: 14pt; color:#ccc;"></i>
                                            @else
                                                <i class="menu-icon mdi mdi-delete-forever" onclick="eliminarAdmin({{$admin->id}})" style="font-size: 14.5pt"></i>
                                            @endif
                                        </td>
                                    </tr>

                                    <div class="modal prueba" id="editAdmin{{$admin->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content" style="background-color: #fff !important;">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cambiar foto de perfil</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-6">
                                                                <img src="data:image/png;base64,{{$admin->profile_url}}" alt="Cloudy Sky" style="max-width: 100%;
                                                                max-height: 100%;
                                                                display: block;">
                                                            </div>
                                                            <div class="col-md-3"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="comment">Suba una nueva foto</label>
                                                        <div class="input-group">
                                                            <input type="hidden" name="user_id" id="user_id_{{$admin->id}}" value="{{$admin->id}}">
                                                            <input type="file" name="foto_{{$admin->id}}" id="foto_{{$admin->id}}" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" onclick="uploadFile({{$admin->id}})">Cambiar</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">No hay datos que mostrar.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>

                    <div class="modal fade" id="newAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content" style="background-color: #fff !important;">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Nuevo Admin</h4>
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                </div>
                                <form method="POST" action="{{ route('register_admin') }}">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div id="pb-modalreglog-progressbar"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Nombre</label>
                                            <div class="input-group pb-modalreglog-input-group">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Correo</label>
                                            <div class="input-group pb-modalreglog-input-group">
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <div class="input-group pb-modalreglog-input-group">
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <audio id="audio" controls style="display: none;">
        <source type="audio/mp3" src="{{ asset('assets/audio/alarma.mp3') }}">
    </audio>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

<script>

    $(document).ready(function(){
        setInterval(alarmaDriver, 50000);

        $( "#openModal" ).click(function() {
            $('#newAdmin').appendTo("body").modal('show');
        });
    });

    function editModal(id){
        $('#editAdmin'+id).appendTo("body").modal('show');
    }

    function eliminarAdmin(id) {
        Swal.fire({
            title: 'Quieres eliminar este administrador?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero eliminarlo! '
        }).then((result) => {
            if (result.isConfirmed) {

                let _token   = $('meta[name="csrf-token"]').attr('content');

                /*Codigo ajax*/
                $.ajax({
                    url:"{{ route('delete_admin') }}",
                    type:"POST",
                    data:{
                        id: id,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success == true) {
                            Swal.fire({
                                icon: 'success',
                                title: response.data,
                                showConfirmButton: true,
                                timer: 5000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            })
                        }else{
                            Swal.fire(
                                'Activo!',
                                response.data,
                                'Cancelled'
                            )
                        }
                    },
                });
            }
        })
    }

    function uploadFile(id){
        
        Swal.fire({
            title: 'Quieres cambiar la foto de perfil?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero cambiarla!'
        }).then((result) => {
            if (result.isConfirmed) {
                let _token   = $('meta[name="csrf-token"]').attr('content');
                let photo = document.getElementById("foto_"+id).files[0];
                
                var file_data = $("#foto_"+id).prop("files")[0];   // Getting the properties of file from file field
                var form_data = new FormData();                  // Creating object of FormData class
                form_data.append("photo", file_data)              // Appending parameter named file with properties of file_field to form_data
                form_data.append("user_id", id)
                form_data.append("_token", "{{ csrf_token() }}")
                //var id = $("#user_id_"+id).val();
                
                $.ajax({
                    url:"{{ route('change_photo_admin') }}",
                    type:"POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data:form_data,
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'La foto fue cambiada',
                                showConfirmButton: true,
                                timer: 8000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Error!',
                                'No se pudo cambiar la contraseña',
                                'Cancelled'
                            )
                        }
                    }
                });
            }
        })
    }

    function alarmaDriver(){
        let _token   = $('meta[name="csrf-token"]').attr('content');
        var audio = document.getElementById("audio");

        /*Codigo ajax*/
        $.ajax({
            url:"{{ route('verify_new_drivers') }}",
            type:"POST",
            data:{
                _token: "{{ csrf_token() }}",
            },
            success: function(response){
                console.log(response);
                if(response.data > 0){
                    
                    audio.play();
                    audio.loop =true;
                    Swal.fire({
                        icon: 'info',
                        title: 'Hay drivers recien registrados',
                        showConfirmButton: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            audio.pause();
                        }
                    })
                }
            },
        });
    }
</script>