@extends('layout.master')

@push('plugin-styles')
@endpush

<style>
    .form-control {
        display: block;
        width: 100%;
        height: 2.5rem !important;
        padding: 0.875rem 1.375rem;
        font-size: 0.75rem;
        font-weight: 400;
        line-height: 1;
        color: #495057;
        background-color: #ffffff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 8px !important;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    img.zoom {
        width: 350px;
        height: 200px;
        -webkit-transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        -ms-transition: all .2s ease-in-out;
    }
    
    .transition {
        -webkit-transform: scale(1.8); 
        -moz-transform: scale(1.8);
        -o-transform: scale(1.8);
        transform: scale(1.8);
    }
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

@section('content')
<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">LISTA DE CLIENTES</h4>
                <form action="{{ route('search_customer') }}" method="POST" role="search">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="customer_search" placeholder="Search customer">
                        </div>
                        <div class="col-md-1">
                            <input type="submit" class="btn btn-success" value="Buscar">
                        </div>
                        
                        <div class="col-md-1">
                            <a href="{{ route('customers') }}" class="btn btn-primary pull-right">Reset</a>
                        </div>
                    </div>
                </form>
                <br>
                <div id="customer_list" class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                            <th>Dui</th>
                            <th>Foto</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($customers) && $customers->count())
                                @foreach ($customers as $customer)
                                    <tr>
                                        <td>{{$customer->id}}</td>
                                        <td>{{$customer->name}}</td>
                                        <td>{{$customer->email}}</td>
                                        <td>{{$customer->phone}}</td>
                                        <td>
                                            @if(empty($customer->dui))
                                                No provisto
                                            @else
                                                {{$customer->dui}}
                                            @endif
                                        </td>
                                        <td>
                                            @if(empty($customer->photo))
                                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgBhcplevwUKGRs1P-Ps8Mwf2wOwnW_R_JIA&usqp=CAU" width="100">
                                            @else
                                                <img src="{{$customer->photo}}" width="100" class="zoom">
                                            @endif
                                        </td>
                                        <td>
                                            @if($customer->status == 0)
                                                <label class="badge badge-danger">Inactivo</label>
                                            @else
                                                <label class="badge badge-success">Activo</label>
                                            @endif
                                        </td>
                                        <td>
                                            <i class="menu-icon mdi mdi-key" data-toggle="modal" data-target="#changePassword{{$customer->id}}"  style="font-size: 14pt" title="Cambiar Contraseña"></i>
                                            &nbsp;&nbsp;&nbsp;
                                            @if($customer->status == 0)
                                                <i class="menu-icon mdi mdi-account-check" onclick="activateCustomer({{$customer->id}})" style="font-size: 15.5pt" title="Activar Usuario"></i>
                                            @else
                                                <i class="menu-icon mdi mdi-block-helper" onclick="desactivateCustomer({{$customer->id}})" style="font-size: 14.5pt" title="Desactivar Usuario"></i>
                                            @endif
                                            &nbsp;&nbsp;&nbsp;
                                            @if($customer->suspended == 0)
                                                <i class="menu-icon mdi mdi mdi-account-off" data-toggle="modal" data-target="#suspendedCustomer{{$customer->id}}" style="font-size: 15.5pt; color:red;" title="Suspender Cliente"></i>
                                            @endif
                                            &nbsp;&nbsp;&nbsp;
                                            <i class="menu-icon mdi mdi-message-text" data-toggle="modal" data-target="#sendEmail{{$customer->id}}"  style="font-size: 14pt" title="Enviar correo"></i>
                                            
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="suspendedCustomer{{$customer->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content" style="background-color: #fff !important;">
                                                <div class="modal-header">
                                                    <h5>Suspender a cliente {{$customer->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                </div>

                                                <div class="modal-body">
                                                    <input type="hidden" name="user_id" id="user_id_{{$customer->id}}" value="{{$customer->id}}">
                                                    <div class="form-group">
                                                        <label for="email">Razón de suspensión</label>
                                                        <div class="input-group pb-modalreglog-input-group">
                                                            <select name="reason" id="reason_{{$customer->id}}" class="form-control" style="height: 2.8rem !important;">
                                                                <option value="Mal trato / Insulto">Mal trato / Insulto</option>
                                                                <option value="Involucrado en robo">Involucrado en robo</option>
                                                                <option value="Irrespetó normas">Irrespetó normas</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="comment">Explique lo sucedido</label>
                                                        <div class="input-group">
                                                            <input type="text" name="comentario_{{$customer->id}}" id="comentario_{{$customer->id}}" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" onclick="suspenderCustomer({{$customer->id}})">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal" id="changePassword{{$customer->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title">Cambiar contraseña</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="comment">Ingrese la nueva contraseña</label>
                                                    <div class="input-group">
                                                        <input type="hidden" name="user_id_pwd" id="user_id_pwd{{$customer->id}}" value="{{$customer->id}}">
                                                        <input type="text" name="password_{{$customer->id}}" id="password_{{$customer->id}}" class="form-control" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-primary" onclick="changePwd({{$customer->id}})">Cambiar</button>
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            </div>
                                          </div>
                                        </div>
                                    </div>

                                    <div class="modal" id="sendEmail{{$customer->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title">Correo para cliente</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="comment">Ingrese el mensaje</label>
                                                    <div class="input-group">
                                                        <input type="hidden" name="name_customer_{{$customer->id}}" id="name_customer_{{$customer->id}}" value="{{$customer->name}}">
                                                        <input type="text" name="message_{{$customer->id}}" id="message_{{$customer->id}}" class="form-control" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-primary" onclick="sendMessage({{$customer->id}})">Enviar</button>
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            </div>
                                          </div>
                                        </div>
                                    </div>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">No hay datos que mostrar.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $customers->links() !!}
                </div>
            </div>
        </div>
    </div>
    <audio id="audio" controls style="display: none;">
        <source type="audio/mp3" src="{{ asset('assets/audio/alarma.mp3') }}">
    </audio>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

<script>

    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#customer_list tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $('.zoom').hover(function() {
            $(this).addClass('transition');
        }, function() {
            $(this).removeClass('transition');
        });

        setInterval(alarmaDriver, 50000);
    });

    function desactivateCustomer(id) {
        Swal.fire({
            title: 'Quieres desactivar este cliente?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero desactivarlo! '
        }).then((result) => {
            if (result.isConfirmed) {

                let _token   = $('meta[name="csrf-token"]').attr('content');

                /*Codigo ajax*/
                $.ajax({
                    url:"{{ route('desactivate_customer') }}",
                    type:"POST",
                    data:{
                        id: id,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success == true) {
                            Swal.fire({
                                icon: 'success',
                                title: response.data,
                                showConfirmButton: true,
                                timer: 5000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            })
                        }else{
                            Swal.fire(
                                'Activo!',
                                response.data,
                                'Cancelled'
                            )
                        }
                    },
                });
            }
        })
    }

    function activateCustomer(id) {
        Swal.fire({
            title: 'Quieres activar este cliente?',
            text: "Podrá pedir viajes después de esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero activarlo! '
        }).then((result) => {
            if (result.isConfirmed) {

                let _token   = $('meta[name="csrf-token"]').attr('content');

                /*Codigo ajax*/
                $.ajax({
                    url:"{{ route('activate_customer') }}",
                    type:"POST",
                    data:{
                        id: id,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success == true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'El cliente seleccionado fue activado',
                                showConfirmButton: true,
                                timer: 5000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            })
                        }else{
                            Swal.fire(
                                'Activo!',
                                'El cliente seleccionado no fue activado.',
                                'Cancelled'
                            )
                        }
                    },
                });
            }
        })
    }

    function suspenderCustomer(id){
        
        Swal.fire({
            title: 'Quieres suspender este cliente?',
            text: "No podrá pedir viajes después de esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero suspenderlo!'
        }).then((result) => {
            if (result.isConfirmed) {
                let _token   = $('meta[name="csrf-token"]').attr('content');
                //var id = $("#user_id_"+id).val();
                $.ajax({
                    url:"{{ route('suspend_user') }}",
                    type:"POST",
                    data:{
                    user_id: id,
                    reason: $("#reason_"+id).val(),
                    comment: $("#comentario_"+id).val(),
                    _token: "{{ csrf_token() }}",
                    },
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'El cliente seleccionado fue suspendido',
                                showConfirmButton: true,
                                timer: 8000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Error!',
                                'El cliente seleccionado no pudo fue suspendido.',
                                'Cancelled'
                            )
                        }
                    }
                });
            }
        })
    }

    function changePwd(id){
        
        Swal.fire({
            title: 'Quieres cambiar la contraseña del cliente?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero cambiarla!'
        }).then((result) => {
            if (result.isConfirmed) {
                let _token   = $('meta[name="csrf-token"]').attr('content');
                //var id = $("#user_id_"+id).val();
                $.ajax({
                    url:"{{ route('change_pwd') }}",
                    type:"POST",
                    data:{
                    user_id: id,
                    password: $("#password_"+id).val(),
                    _token: "{{ csrf_token() }}",
                    },
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'La contraseña fue cambiada',
                                showConfirmButton: true,
                                timer: 8000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Error!',
                                'No se pudo cambiar la contraseña',
                                'Cancelled'
                            )
                        }
                    }
                });
            }
        })
    }

    function sendMessage(id){
        
        Swal.fire({
            title: 'Quieres enviar email al cliente?',
            //text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, quiero enviarlo!'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    icon: 'success',
                    title: 'El correo fue enviado',
                    showConfirmButton: true,
                    timer: 8000
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }else{
                        location.reload();
                    }
                })
                /*let _token   = $('meta[name="csrf-token"]').attr('content');
                //var id = $("#user_id_"+id).val();
                $.ajax({
                    url:"{{ route('send_notification') }}",
                    type:"POST",
                    data:{
                        user_id: id,
                        name_driver: $("#name_driver_"+id).val(),
                        message: $("#message_"+id).val(),
                        _token: "{{ csrf_token() }}",
                    },
                    success:function(response){
                        console.log(response);
                        if(response.success == true) {

                            Swal.fire({
                                icon: 'success',
                                title: 'La notificación fue enviada',
                                showConfirmButton: true,
                                timer: 8000
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            })

                        }else{
                            Swal.fire(
                                'Error!',
                                response.success,
                                'Cancelled'
                            )
                        }
                    }
                });*/
            }
        })
    }

    function alarmaDriver(){
        let _token   = $('meta[name="csrf-token"]').attr('content');
        var audio = document.getElementById("audio");

        /*Codigo ajax*/
        $.ajax({
            url:"{{ route('verify_new_drivers') }}",
            type:"POST",
            data:{
                _token: "{{ csrf_token() }}",
            },
            success: function(response){
                console.log(response);
                if(response.data > 0){
                    
                    audio.play();
                    audio.loop =true;
                    Swal.fire({
                        icon: 'info',
                        title: 'Hay usuarios recien registrados',
                        showConfirmButton: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            audio.pause();
                        }
                    })
                }
            },
        });
    }

</script>