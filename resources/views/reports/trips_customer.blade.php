@extends('layout.master')

@push('plugin-styles')
@endpush

<style>
    .select-control {
        display: block;
        width: 100%;
        height: 2rem;
        padding: 0.5px;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1;
        color: #495057;
        background-color: #ffffff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 2px;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

@section('content')
<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">LISTA DE VIAJES</h4>
                <div class="row">
                    <div class="col-md-8">
                        <form method="POST" role="search">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="name_search" id="name_search" placeholder="Buscar cliente">
                                </div>
                                <div class="col-md-1" style="padding: 3px;">
                                    <input type="button" id="sendSearch" class="btn btn-success" value="Buscar">
                                </div>
                                
                                <div class="col-md-1" style="padding: 3px;">
                                    <a href="{{ route('report_trips') }}" class="btn btn-primary pull-right">Reset</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-1" style="text-align: right; padding: 5px;">
                        <b>Filtro</b>
                    </div>
                    <div class="col-md-3">
                        <select class="select-control" name="type_report" id="type_report">
                            <option value="day" selected>Día</option>
                            <option value="week">Semana</option>
                            <option value="month">Mes</option>
                        </select>
                    </div>
                </div>
                <br>
                <div id="customer_list" class="table-responsive">
                    <table class="table table-hover" id="trips_list">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Cliente</th>
                            <th>Driver</th>
                            <th>Factura</th>
                            <th>Costo</th>
                            <th>Pago</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody id="bodyrow">
                            @if(!empty($trips) && $trips->count())
                                @foreach ($trips as $trip)
                                    <tr >
                                        <td>{{$trip->id}}</td>
                                        <td>{{$trip->customer_name}}</td>
                                        <td>{{$trip->driver_name}}</td>
                                        <td>{{$trip->bill_trip}}</td>
                                        <td>${{number_format((float)$trip->cost, 2, '.', '')}}</td>
                                        <td>{{$trip->payment}}</td>
                                        <td>{{$trip->created_at}}</td>
                                        
                                        <td>
                                            &nbsp;&nbsp;
                                            <i class="menu-icon mdi mdi-file-find" style="font-size: 15.5pt" title="Ver más datos"></i>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">No hay datos que mostrar.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $trips->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

<script>

$(document).on('change','#type_report',function(){
    var valor;
    let _token = $('meta[name="csrf-token"]').attr('content');
    valor = $(this).find(":selected").val();

    $.ajax({
        url:"{{ route('filter_date_trip') }}",
        type:"POST",
        data:{
            filter : valor,
            _token: "{{ csrf_token() }}"
        },
        success:function(response){
            if(response.success){
                console.log(response.data.data);
                $('#trips_list tbody').empty();
                $.each(response.data.data, function( key, value ) {
                    $('#trips_list').append('<tr> <td>'+ value.id +'</td> <td>'+ value.customer_name +'</td> <td>'+ value.driver_name +'</td> <td>'+ value.bill_trip +'</td> <td>$'+ value.cost.toFixed(2) +'</td> <td>'+ value.payment +'</td> <td>'+ value.created_at +'</td> <td><i class="menu-icon mdi mdi-file-find" style="font-size: 15.5pt" title="Ver más datos"></i></td> </tr>');
                });
            }
            
        }
    });
});


$(document).on('click', '#sendSearch', function() {

    if(!$("#name_search").val()){
        alert("Debe ingresar el nombre de un cliente");
    }else{
        let _token = $('meta[name="csrf-token"]').attr('content');
        valor = $("#name_search").val();

        $.ajax({
            url:"{{ route('filter_trips_customer') }}",
            type:"POST",
            data:{
                search : valor,
                _token: "{{ csrf_token() }}"
            },
            success:function(response){
                if(response.success){
                    console.log(response.data.data);
                    $('#trips_list tbody').empty();
                    $.each(response.data.data, function( key, value ) {
                        $('#trips_list').append('<tr> <td>'+ value.id +'</td> <td>'+ value.customer_name +'</td> <td>'+ value.driver_name +'</td> <td>'+ value.bill_trip +'</td> <td>$'+ value.cost.toFixed(2) +'</td> <td>'+ value.payment +'</td> <td>'+ value.created_at +'</td> <td><i class="menu-icon mdi mdi-file-find" style="font-size: 15.5pt" title="Ver más datos"></i></td> </tr>');
                    });
                }
                
            }
        });
    }

});

</script>
