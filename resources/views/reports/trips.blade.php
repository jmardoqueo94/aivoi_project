@extends('layout.master')

@push('plugin-styles')
@endpush

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

@section('content')
<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">LISTA DE VIAJES</h4>
                <div class="row">

                
                    <div class="col-md-6">
                        <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
                    </div>
                    <div class="col-md-6 ">
                        <select class="form-control" name="type_report" id="type_report">
                            <option value="" selected>Día</option>
                            <option value="">Semana</option>
                            <option value="">Mes</option>
                        </select>
                    </div>
                </div>
                <br>
                <div id="customer_list" class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Factura</th>
                            <th>Costo</th>
                            <th>Pago</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($trips) && $trips->count())
                                @foreach ($trips as $trip)
                                    <tr>
                                        <td>{{$trip->id}}</td>
                                        <td>{{$trip->bill_trip}}</td>
                                        <td>{{$trip->cost}}</td>
                                        <td>{{$trip->payment_id}}</td>
                                        
                                        <td>
                                            <i class="menu-icon mdi mdi-lead-pencil" style="font-size: 14pt"></i>
                                            &nbsp;&nbsp;&nbsp;
                                            <i class="menu-icon mdi mdi-account-check" style="font-size: 15.5pt" title="Activar Usuario"></i>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">No hay datos que mostrar.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $trips->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush
