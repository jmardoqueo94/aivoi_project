@extends('layout.master')

@push('plugin-styles')
@endpush

<link href="https://api.tiles.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.css" rel="stylesheet" />

<style>
    body {
        margin: 0;
        padding: 0;
    }

    #map {
        top: 0;
        bottom: 0;
        width: 100%;
        height: 500px;
    }
    
    .marker {
        background-image: url('../assets/images/icon_car.png');
        background-size: cover;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        cursor: pointer;
    }

    .mapboxgl-popup {
        max-width: 200px;
    }

    .mapboxgl-popup-content {
        text-align: center;
        font-family: 'Open Sans', sans-serif;
    }
</style>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>
<script src="https://api.tiles.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.js"></script>

@section('content')
<div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Rastreo de drivers</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoibWFydjAwNyIsImEiOiJja2o4dDl4cWs2dWx5MnZwZGExanA4czA0In0.vLlAiNRX57YBvo0iXWahsw';

    $( document ).ready(function() {

        $.ajax({
            url:"{{ route('get_coords') }}",
            type:"GET",
            success:function(response){
                if(response.success){
                    console.log(response.data);
                    geojson = response.data;

                    var map = new mapboxgl.Map({
                        container: 'map',
                        style: 'mapbox://styles/mapbox/light-v10',
                        center: [-89.2258195, 13.7055247],
                        zoom: 12
                    });

                    $.each(geojson, function (key, val) {

                        val.features.forEach(function (marker) {
                            // create a HTML element for each feature
                            var el = document.createElement('div');
                            el.className = 'marker';

                            var busy;
                            if(marker.properties.busy == 1){
                                busy = "<p style='color:red;'>En viaje</p>";
                            }else{
                                busy = "<p style='color:green;'>Disponible</p>";
                            }

                            // make a marker for each feature and add it to the map
                            new mapboxgl.Marker(el)
                            .setLngLat(marker.geometry.coordinates)
                            .setPopup(
                                new mapboxgl.Popup({ offset: 25 }) // add popups
                                .setHTML(
                                    '<h3>' +
                                    marker.properties.title +
                                    '</h3><p>' +
                                    marker.properties.description +
                                    '</p><p>' +
                                    marker.properties.phone +
                                    '</p><p>' +
                                    busy +
                                    '</p>'
                                )
                            )
                            .addTo(map);
                        });
                    });
                }
            }
        });

        setInterval(maps, 60000);
    });

    function maps(){
        var geojson;
        $.ajax({
            url:"{{ route('get_coords') }}",
            type:"GET",
            success:function(response){
                if(response.success){
                    console.log(response.data);
                    geojson = response.data;

                    var map = new mapboxgl.Map({
                        container: 'map',
                        style: 'mapbox://styles/mapbox/light-v10',
                        center: [-89.2258195, 13.7055247],
                        zoom: 13
                    });

                    $.each(geojson, function (key, val) {
                        
                        val.features.forEach(function (marker) {
                            // create a HTML element for each feature
                            var el = document.createElement('div');
                            el.className = 'marker';

                            // make a marker for each feature and add it to the map
                            new mapboxgl.Marker(el)
                            .setLngLat(marker.geometry.coordinates)
                            .setPopup(
                                new mapboxgl.Popup({ offset: 25 }) // add popups
                                .setHTML(
                                    '<h3>' +
                                    marker.properties.title +
                                    '</h3><p>' +
                                    marker.properties.description +
                                    '</p>'
                                )
                            )
                            .addTo(map);
                        });

                    });
                }
            }
        });
    }

    
</script>