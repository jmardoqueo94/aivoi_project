<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
  </head>
  <body>
    <p>
      <img src='https://res.cloudinary.com/grupo-dicase/image/upload/v1610396165/bienvenida/usuario_pssywj.jpg'>
      <br>
      <strong><h2>¡Gracias por viajar con!</h2> </strong> 
        <br>
        <h3>{{ $name_customer }}</h3>
        El total de tu viaje fue {{$cost}}
        <br>
        <br>
      <strong>
        #AivoiTeMueve
      </strong>
    </p>
  </body>
</html>