<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
  </head>
  <body>
    <h2>Recuperación de contraseña</h2>
    <p>
        
            <img src='https://res.cloudinary.com/grupo-dicase/image/upload/v1622687518/aivoi%20app%20resourses/AIVOI_LOGO-02_xp5pup_ta6d3j.png' width='200px'>
            <br>
            <strong><h2>Tu clave temporal: {{ $test_message }} </h2> </strong> <br>
            Esta clave te servirá para iniciar sesión, una vez dentro de la aplicación puedes cambiarla.
            <br>
            <br>
            <b>#AivoiTeMueve</b>
    </p>
  </body>
</html>