<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UI\DashBoardController@getDataDashboard')->name('/');

Route::get('login', function () { return view('pages.user-pages.login'); })->name('login');

Route::post('login_post', 'UI\LoginController@loginUI')->name('login_post');

Route::get('logout', function ()
{
    auth()->logout();
    Session()->flush();

    return Redirect::to('login');
})->name('logout');

Route::post('test_notification', 'Controller@testNotification')->name('test_notification');

Route::group(['prefix' => 'users', 'middleware' => 'auth'], function(){

    /* RUTAS PARA MOSTRAR DATOS */
    Route::get('drivers', 'UI\UserController@getDrivers')->name('drivers');
    Route::get('customers', 'UI\UserController@getCustomers')->name('customers');
    Route::get('admin', 'UI\UserController@getAdmins')->name('admin');
    Route::get('suspended', 'UI\UserController@getSuspendedUser')->name('suspended');

    /* RUTAS PARA EL DASHBOARD */
    Route::get('dashboard_data', 'UI\DashBoardController@getDataDashboard')->name('dashboard_data');

    /* RUTAS PARA DRIVER */
    Route::get('update_table', 'UI\UserController@getUpdatedTable')->name('update_table');

    Route::post('activate_driver', 'UI\UserController@activateDriver')->name('activate_driver');

    Route::post('desactivate_driver', 'UI\UserController@desactivateDriver')->name('desactivate_driver');

    /* RUTAS PARA ADMIN */
    Route::post('register_admin', 'UI\UserController@registerAdmin')->name('register_admin');

    Route::post('delete_admin', 'UI\UserController@deleteAdmin')->name('delete_admin');

    /* RUTAS PARA CUSTOMER */
    Route::post('desactivate_customer', 'UI\UserController@desactivateCustomer')->name('desactivate_customer');

    Route::post('activate_customer', 'UI\UserController@activateCustomer')->name('activate_customer');

    /* RUTAS PARA BUSQUEDA DE USUARIOS */
    Route::post('search_driver', 'UI\UserController@searchDriver')->name('search_driver');

    Route::post('search_customer', 'UI\UserController@searchCustomer')->name('search_customer');

    Route::post('suspend_user', 'UI\UserController@suspendUser')->name('suspend_user');

    Route::post('change_pwd', 'UI\UserController@changePwd')->name('change_pwd');

    Route::post('revalidate_user', 'UI\UserController@revalidateUser')->name('revalidate_user');

    Route::post('send_notification', 'UI\UserController@sendNotificationDriver')->name('send_notification');

    Route::post('change_photo_admin', 'UI\UserController@changeAdminPhoto')->name('change_photo_admin');

    /* RUTA PARA VERIFICAR SI HAY NUEVOS DRIVERS */
    Route::post('verify_new_drivers', 'UI\UserController@verifyNewUsers')->name('verify_new_drivers');

});

/* RUTAS PARA MAPA DE TRACKING */
Route::group(['prefix' => 'track', 'middleware' => 'auth'], function(){

    Route::get('track_driver', 'UI\TrackingController@mapTracking')->name('track_driver');
        
    Route::get('get_coords', 'UI\TrackingController@getCoords')->name('get_coords');

});

Route::group(['prefix' => 'emprendedores', 'middleware' => 'auth'], function(){

    Route::get('index', 'UI\EntrepreneursController@index')->name('index');

    Route::post('register_entrepreneurs', 'UI\EntrepreneursController@register')->name('register_entrepreneurs');

    Route::post('edit_entrepreneurs', 'UI\EntrepreneursController@editEmprendedor')->name('edit_entrepreneurs');

    Route::post('delete_emprendedor', 'UI\EntrepreneursController@deleteEmprendedor')->name('delete_emprendedor');

});

Route::group(['prefix' => 'settings', 'middleware' => 'auth'], function(){

    Route::get('index', 'UI\SettingsController@index')->name('index');

    Route::post('edit_settings', 'UI\SettingsController@editSettings')->name('edit_settings');

});

/* RUTAS PARA REPORTS */
Route::group(['prefix' => 'reports', 'middleware' => 'auth'], function(){
    
    Route::get('report_trips', 'UI\ReportsController@getTrips')->name('report_trips');

    Route::get('report_trips_driver', 'UI\ReportsController@getTripsDriver')->name('report_trips_driver');

    Route::post('search_name_trip', 'UI\ReportsController@searchNametrip')->name('search_name_trip');

    Route::post('filter_date_trip', 'UI\ReportsController@filteredDateTrips')->name('filter_date_trip');
    
    Route::post('filter_trips_customer', 'UI\ReportsController@searchCustomerTrips')->name('filter_trips_customer');

    Route::post('filter_trips_driver', 'UI\ReportsController@searchDriverTrips')->name('filter_trips_driver');

});


Route::group(['prefix' => 'basic-ui'], function(){
    Route::get('accordions', function () { return view('pages.basic-ui.accordions'); });
    Route::get('buttons', function () { return view('pages.basic-ui.buttons'); });
    Route::get('badges', function () { return view('pages.basic-ui.badges'); });
    Route::get('breadcrumbs', function () { return view('pages.basic-ui.breadcrumbs'); });
    Route::get('dropdowns', function () { return view('pages.basic-ui.dropdowns'); });
    Route::get('modals', function () { return view('pages.basic-ui.modals'); });
    Route::get('progress-bar', function () { return view('pages.basic-ui.progress-bar'); });
    Route::get('pagination', function () { return view('pages.basic-ui.pagination'); });
    Route::get('tabs', function () { return view('pages.basic-ui.tabs'); });
    Route::get('typography', function () { return view('pages.basic-ui.typography'); });
    Route::get('tooltips', function () { return view('pages.basic-ui.tooltips'); });
});

Route::group(['prefix' => 'advanced-ui'], function(){
    Route::get('dragula', function () { return view('pages.advanced-ui.dragula'); });
    Route::get('clipboard', function () { return view('pages.advanced-ui.clipboard'); });
    Route::get('context-menu', function () { return view('pages.advanced-ui.context-menu'); });
    Route::get('popups', function () { return view('pages.advanced-ui.popups'); });
    Route::get('sliders', function () { return view('pages.advanced-ui.sliders'); });
    Route::get('carousel', function () { return view('pages.advanced-ui.carousel'); });
    Route::get('loaders', function () { return view('pages.advanced-ui.loaders'); });
    Route::get('tree-view', function () { return view('pages.advanced-ui.tree-view'); });
});

Route::group(['prefix' => 'forms'], function(){
    Route::get('basic-elements', function () { return view('pages.forms.basic-elements'); });
    Route::get('advanced-elements', function () { return view('pages.forms.advanced-elements'); });
    Route::get('dropify', function () { return view('pages.forms.dropify'); });
    Route::get('form-validation', function () { return view('pages.forms.form-validation'); });
    Route::get('step-wizard', function () { return view('pages.forms.step-wizard'); });
    Route::get('wizard', function () { return view('pages.forms.wizard'); });
});

Route::group(['prefix' => 'editors'], function(){
    Route::get('text-editor', function () { return view('pages.editors.text-editor'); });
    Route::get('code-editor', function () { return view('pages.editors.code-editor'); });
});

Route::group(['prefix' => 'charts'], function(){
    Route::get('chartjs', function () { return view('pages.charts.chartjs'); });
    Route::get('morris', function () { return view('pages.charts.morris'); });
    Route::get('flot', function () { return view('pages.charts.flot'); });
    Route::get('google-charts', function () { return view('pages.charts.google-charts'); });
    Route::get('sparklinejs', function () { return view('pages.charts.sparklinejs'); });
    Route::get('c3-charts', function () { return view('pages.charts.c3-charts'); });
    Route::get('chartist', function () { return view('pages.charts.chartist'); });
    Route::get('justgage', function () { return view('pages.charts.justgage'); });
});

Route::group(['prefix' => 'tables'], function(){
    Route::get('basic-table', function () { return view('pages.tables.basic-table'); });
    Route::get('data-table', function () { return view('pages.tables.data-table'); });
    Route::get('js-grid', function () { return view('pages.tables.js-grid'); });
    Route::get('sortable-table', function () { return view('pages.tables.sortable-table'); });
});

Route::get('notifications', function () {
    return view('pages.notifications.index');
});

Route::group(['prefix' => 'icons'], function(){
    Route::get('material', function () { return view('pages.icons.material'); });
    Route::get('flag-icons', function () { return view('pages.icons.flag-icons'); });
    Route::get('font-awesome', function () { return view('pages.icons.font-awesome'); });
    Route::get('simple-line-icons', function () { return view('pages.icons.simple-line-icons'); });
    Route::get('themify', function () { return view('pages.icons.themify'); });
});

Route::group(['prefix' => 'maps'], function(){
    Route::get('vector-map', function () { return view('pages.maps.vector-map'); });
    Route::get('mapael', function () { return view('pages.maps.mapael'); });
    Route::get('google-maps', function () { return view('pages.maps.google-maps'); });
});

Route::group(['prefix' => 'user-pages'], function(){
    Route::get('login', function () { return view('pages.user-pages.login'); });
    Route::get('login-2', function () { return view('pages.user-pages.login-2'); });
    Route::get('multi-step-login', function () { return view('pages.user-pages.multi-step-login'); });
    Route::get('register', function () { return view('pages.user-pages.register'); });
    Route::get('register-2', function () { return view('pages.user-pages.register-2'); });
    Route::get('lock-screen', function () { return view('pages.user-pages.lock-screen'); });
});


Route::group(['prefix' => 'general-pages'], function(){
    Route::get('blank-page', function () { return view('pages.general-pages.blank-page'); });
    Route::get('landing-page', function () { return view('pages.general-pages.landing-page'); });
    Route::get('profile', function () { return view('pages.general-pages.profile'); });
    Route::get('email-templates', function () { return view('pages.general-pages.email-templates'); });
    Route::get('faq', function () { return view('pages.general-pages.faq'); });
    Route::get('faq-2', function () { return view('pages.general-pages.faq-2'); });
    Route::get('news-grid', function () { return view('pages.general-pages.news-grid'); });
    Route::get('timeline', function () { return view('pages.general-pages.timeline'); });
    Route::get('search-results', function () { return view('pages.general-pages.search-results'); });
    Route::get('portfolio', function () { return view('pages.general-pages.portfolio'); });
    Route::get('user-listing', function () { return view('pages.general-pages.user-listing'); });
});

Route::group(['prefix' => 'ecommerce'], function(){
    Route::get('invoice', function () { return view('pages.ecommerce.invoice'); });
    Route::get('invoice-2', function () { return view('pages.ecommerce.invoice-2'); });
    Route::get('pricing', function () { return view('pages.ecommerce.pricing'); });
    Route::get('product-catalogue', function () { return view('pages.ecommerce.product-catalogue'); });
    Route::get('project-list', function () { return view('pages.ecommerce.project-list'); });
    Route::get('orders', function () { return view('pages.ecommerce.orders'); });
});

// For Clear cache
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

// 404 for undefined routes
/*Route::any('/{page?}',function(){
    return View::make('pages.error-pages.error-404');
})->where('page','.*');*/