<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('verify', 'TripsController@verifyTripInfo')->name('verify');

//Ruta para login
Route::post('login_user', 'Auth\LoginController@login')->name('login_user');

//Ruta para registrar usuario
Route::post('register_user', 'Auth\RegisterController@register')->name('register_user');

//Ruta para obtener tipos de carros
Route::get('type_cars', 'DriverController@getTypeCars')->name('type_cars');

//Ruta para obtener marcas y modelos de carros
Route::get('model_cars', 'DriverController@getModelsCars')->name('model_cars');

//Ruta para obtener tipos de pago
Route::get('type_payments', 'PaymentController@getPayments')->name('type_payments');

//Ruta de pruebas para email
Route::post('test_email', 'MailController@createAccount')->name('test_email');

//Rutas para resetear password
Route::post('reset_password', 'UserController@resetPassword');

Route::get('migrate', 'MigrateController@migrate')->name('migrate');

Route::get('migrate_users', 'MigrateController@migrateUsers')->name('migrate_users');

Route::get('cars', 'MigrateController@activeCars')->name('cars');

Route::get('total_users', 'MailController@TotalUsers')->name('total_users');

Route::get('get_banks', 'PaymentController@getBanks')->name('get_banks');

//Ruta de pruebas para email
Route::post('create_account_driver', 'MailController@createAccountDriver')->name('create_account_driver');

Route::group(['middleware' => 'auth.jwt'], function () {

    //Ruta para cerrar sesion
    Route::post('logout', 'Auth\LoginController@logout');

    //Ruta para poner online u offline al driver para recibir viajes
    Route::post('online_driver', 'DriverController@onlineDriver')->name('online_driver');

    //Ruta para obtener datos de un cliente por id
    Route::post('get_user_data', 'UserController@getUserData')->name('get_user_data');

    //Rutas para clientes
    Route::get('get_customers', 'UserController@getCustomers')->name('get_customers');

    //Rutas para drivers
    Route::get('get_drivers', 'UserController@getDrivers')->name('get_drivers');

    //Rutas para obtener datos de un cliente
    Route::post('get_customer_data', 'UserController@getCustomerData')->name('get_customer_data');

    //Ruta para obtener datos de un driver
    Route::post('get_driver_data', 'UserController@getDriverData')->name('get_driver_data');

    //Ruta para eliminar un usuario
    Route::post('desactivate_user', 'UserController@desactivateUser')->name('desactivate_user');

    //Ruta para cambiar la contraseña
    Route::post('change_password', 'UserController@changePassword')->name('change_password');

    //Ruta para actualzar datos de usuario
    Route::post('update_customer', 'CustomerController@updateCustomer')->name('update_customer');

    //Ruta para guardar direcciones de clientes
    Route::post('save_address', 'CustomerController@saveAddress')->name('save_address');

    //Ruta para actualizar direcciones de clientes
    Route::post('update_address', 'CustomerController@updateAddress')->name('update_address');

    //Ruta para borrar direcciones de clientes
    Route::post('delete_address', 'CustomerController@deleteAddress')->name('delete_address');

    //Ruta para obtener las direcciones guardadas por cliente
    Route::post('get_customer_address', 'CustomerController@getCustomerAddresses')->name('get_customer_address');

    //Ruta para actualizar datos de driver
    Route::post('update_driver', 'DriverController@updateDriver')->name('update_driver');

    //Ruta para agregar un carro a driver
    Route::post('add_car_driver', 'DriverController@addCarDriver')->name('add_car_driver');

    //Ruta para actualizar datos de un carro de driver
    Route::post('update_car_driver', 'DriverController@updateCarDriver')->name('update_car_driver');

    //Ruta para obtener los drivers cerca de un cliente
    Route::post('get_drivers_near', 'DriverController@getDriversNear')->name('get_drivers_near');

    //Ruta para obtener los carros que ha registrado un driver
    Route::post('get_cars_driver', 'DriverController@getDriverCars')->name('get_cars_driver');

    //Ruta para eliminar un vehiculo
    Route::post('delete_car_driver', 'DriverController@deleteCarDriver')->name('delete_car_driver');

    //Ruta para crear un viaje
    Route::post('create_trip', 'TripsController@createTrip')->name('create_trip');

    //Ruta para cancelar un viaje
    Route::post('cancel_trip', 'TripsController@cancelTrip')->name('cancel_trip');

    //Ruta para rechazar un viaje
    Route::post('reject_trip', 'TripsController@rejectTrip')->name('reject_trip');

    //Ruta para obtener todos los viajes de un cliente
    Route::post('get_trips_customer', 'CustomerController@getTripsCustomer')->name('get_trips_customer');

    //Ruta para obtener los viajes de un driver
    Route::post('get_trips_driver', 'DriverController@getTripsDriver')->name('get_trips_driver');

    Route::post('verify_password', 'UserController@verifyPassword')->name('verify_password');

    Route::post('get_messages', 'MessagesController@getMessages')->name('get_messages');

    Route::post('update_document_driver', 'DriverController@updateDocumentsDriver')->name('update_document_driver');

    /* AQUI IRAN LAS RUTAS PARA CALCULAR EL VALOR DEL VIAJE */
    Route::post('generate_price', 'MetricsController@generatePrice')->name('generate_price');


    //Rutas para notificaciones

    Route::post('request_trip_not', 'TripsController@requestTrip')->name('request_trip_not');

    Route::post('wait_trip_not', 'TripsController@waitTrip')->name('wait_trip_not');

    Route::post('init_trip_not', 'TripsController@initTrip')->name('init_trip_not');

    Route::post('finish_trip_not', 'TripsController@finishTrip')->name('finish_trip_not');

    Route::post('pay_confirmation', 'TripsController@payConfirmation')->name('pay_confirmation');

    Route::post('accept_trip_not', 'TripsController@acceptTrip')->name('accept_trip_not');
    
    Route::post('get_metrics', 'MetricsController@distancia')->name('get_metrics');

    //Ruta para consultar viaje activo de cliente
    Route::post('get_trip_customer', 'TripsController@getTripCustomer')->name('get_trip_customer');

    //Ruta para consultar viaje activo de driver
    Route::post('get_trip_driver', 'TripsController@getTripDriver')->name('get_trip_driver');

    //Rutas para calificar usuarios
    Route::post('calificate_user', 'UserController@calificateUser')->name('calificate_user');

    //Ruta para guardar el balance negativo o positivo de un cliente
    Route::post('balance_user', 'TripsController@balanceUser')->name('balance_user');

    //Ruta para obtener ganancias y total ganado en el día por drivers
    Route::post('total_balance_driver', 'DriverController@getTotalBalanceDriver')->name('total_balance_driver');

    Route::post('invoice', 'InvoiceController@createInvoice')->name('invoice');

    Route::post('set_balance_driver', 'InvoiceController@setBalanceDriver')->name('set_balance_driver');

    //Route::post('cancel_trip_not', 'TripsController@cancelTrip')->name('cancel_trip_not');
});
